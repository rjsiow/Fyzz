import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
/*
  Generated class for the MarkersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MarkersProvider {

  constructor(private afs: AngularFirestore) {
    console.log('Hello MarkersProvider Provider');

  }

  getBarMarkers() {
    console.log('Fetching Collection Data');
    return this.afs.collection('Bar').valueChanges();
  }

  getRestoMarkers() {
    return this.afs.collection('Resto').valueChanges();
  }

  getAdvantageMarkers() {
    return this.afs.collection('Advantage').valueChanges();
  }
}
