import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { NativeStorage } from '@ionic-native/native-storage';
import { AuthService } from '../services/auth.service';
import { Push, PushObject, PushOptions } from '@ionic-native/push';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = 'MapPage';

  language: string;

  constructor(
    platform: Platform,
    public alertCtrl: AlertController,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public translate: TranslateService,
    public nativeStorage: NativeStorage,
    public storage: Storage,
    public events: Events,
    private auth: AuthService,
    private push: Push) {
      platform.ready().then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        statusBar.styleDefault();
        statusBar.backgroundColorByHexString('#000000');
        splashScreen.hide();

        this.initPush();

        this.setLanguage("");
        this.events.subscribe('functionCall:settingsSaved', eventData => {
          this.setLanguage(eventData);
        });

        this.checkFirstTime();
      });
    }

     checkFirstTime(){
       this.storage.get('introShown').then((result) => {
    
         if(result){
           this.rootPage = 'MapPage';
         } else {
           this.rootPage = 'TutorialPage';
           this.storage.set('introShown', true);
         }
    
       });
     }

    setLanguage(lang) {
    if (lang) {
      this.translate.use(lang);
      this.nativeStorage.setItem('langSettings', {
        language: lang
      })
        .then(
          () => console.log('Stored item!'),
          error => console.error('Error storing item', error)
        );
    } else {
      this.nativeStorage.getItem('langSettings')
        .then(
          data => {
            this.language = data.language
            if (this.language) {
              this.translate.setDefaultLang('fr');
              this.translate.use(this.language);
            } else {
              this.translate.setDefaultLang('fr');
              let userLang = navigator.language.split('-')[0];
              console.log("Language: " + userLang);
              userLang = /(fr|en|de|it)/gi.test(userLang) ? userLang : 'fr';
              this.translate.use(userLang);
            }
          },
          error => {
            this.translate.setDefaultLang('fr');
            let userLang = navigator.language.split('-')[0];
            console.log("Language: " + userLang);
            userLang = /(fr|en|de|it)/gi.test(userLang) ? userLang : 'fr';
            this.translate.use(userLang);
            this.nativeStorage.setItem('userSettings', {
              language: userLang
            })
              .then(
                () => console.log('Stored item!'),
                error => console.error('Error storing item', error)
              );
          }
        );
    }
  }


  initPush(){
    const options: PushOptions = {
      android: {
        icon: 'notification_icon'
      },
      ios: {
        alert: 'true',
        badge: 'true',
        sound: 'false'
      }
    };

    const pushObject: PushObject = this.push.init(options);

    pushObject.on('notification').subscribe((notification: any) =>{
      this.translate.get(['APP.NOTIFTITLE'])
      .subscribe(res => {
      //console.log('Received a notification', notification);
        let alert = this.alertCtrl.create({
          title: res["APP.NOTIFTITLE"],
          subTitle: "<div class='subNotif'>" + notification.title + "</div>",
          message: notification.message,
          buttons: ['Ok']
        });
          alert.present();
      })
    });

    pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));

    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));

    this.push.hasPermission()
        .then((res: any) => {
          if (res.isEnabled) {
            console.log('We have permission to send push notifications');
            // this.initPush();
          } else {
            console.log('We do not have permission to send push notifications');
          }
      });
    }
  }
