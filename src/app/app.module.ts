import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

// import { ContactPage } from '../pages/contact/contact';
// import { SettingsPage } from '../pages/settings/settings';
// import { HomePage } from '../pages/home/home';
// import { MapPage } from '../pages/map/map';
// import { LoginPage } from '../pages/login/login';
// import { SignupPage } from '../pages/signup/signup';
// import { ListeBarPage } from '../pages/liste-bar/liste-bar';
// import { RestaurantPage } from '../pages/restaurant/restaurant'
// import { ResetPasswordPage } from '../pages/reset-password/reset-password';
// import { ListeAdvantagePage } from '../pages/liste-advantage/liste-advantage';

// import { HomePageModule } from '../pages/home/home.module';
// import { MapPageModule } from '../pages/map/map.module';
// import { LoginPageModule } from '../pages/login/login.module';
// import { SignupPageModule } from '../pages/signup/signup.module';
// import { ContactPageModule } from '../pages/contact/contact.module';
// import { ResetPasswordPageModule } from '../pages/reset-password/reset-password.module';
// import { ListeBarPageModule } from '../pages/liste-bar/liste-bar.module';
// import { BarPageModule } from '../pages/bar/bar.module';
// import { ListeRestaurantPageModule } from '../pages/liste-restaurant/liste-restaurant.module';
// import { RestaurantPageModule } from '../pages/restaurant/restaurant.module';
// import { ListeAdvantagePageModule } from '../pages/liste-advantage/liste-advantage.module';
// import { AdvantagePageModule } from '../pages/advantage/advantage.module';
// import { SettingsPageModule } from '../pages/settings/settings.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';


import { AlertController } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';

//Firebase stuff
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { firebaseConfig } from './credentials';
import { MarkersProvider } from '../providers/markers/markers';

import { NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { AuthService } from '../services/auth.service';
import { PaymentService } from '../services/payment.service';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
/*import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';*/
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    // HomePageModule,
    // MapPageModule,
    // LoginPageModule,
    // SignupPageModule,
    // ContactPageModule,
    // ResetPasswordPageModule,
    // ListeBarPageModule,
    // BarPageModule,
    // ListeRestaurantPageModule,
    // RestaurantPageModule,
    // ListeAdvantagePageModule,
    // AdvantagePageModule,
    // SettingsPageModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      backButtonIcon: 'ios-arrow-back'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativeStorage,
    AlertController,
    NativeGeocoder,
    BarcodeScanner,
    Push,
    HttpClient,
    Network,
    /*GoogleMaps,
    Geolocation,*/
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MarkersProvider,
    AngularFireAuth,
    AngularFireDatabase,
    AuthService,
    PaymentService
  ]
})
export class AppModule {}
