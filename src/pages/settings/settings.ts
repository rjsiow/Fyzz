import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { Events } from 'ionic-angular';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  language: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translate: TranslateService,
    public nativeStorage: NativeStorage,
    public events: Events) {
      this.getSettings();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }


  public saveSettings() {
    this.nativeStorage.setItem('userSettings', {
      language: this.language
    })
      .then(
        () => console.log('Stored item!'),
        error => console.error('Error storing item', error)
      );
    this.events.publish('functionCall:settingsSaved', this.language);
  }


  public getSettings() {
    this.nativeStorage.getItem('userSettings')
      .then(
        data => {
          this.language = data.language
        },
        error => console.error(error)
      );
  }
}
