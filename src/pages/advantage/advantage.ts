import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';
import { Subscription} from 'rxjs/Subscription';


@IonicPage()
@Component({
  selector: 'page-advantage',
  templateUrl: 'advantage.html',
})


export class AdvantagePage {
  userLang: string;

  networkConnection: boolean;
  networkConnected: Subscription;
  networkDisconnected: Subscription;

  advAddress: string;
  advCity: string;
  advName: string;
  advantageInfo: any;
  advAdvantage: string;
  advCode: string;
  description: string;
  promo: string;
  photoName1: string;
  photoName2: string;
  photoName3: string;
  photo1;
  photo2;
  photo3;
  advId: string;
  show_hours = false;

  advInfoCollection: any;
  advInfoObs: any;

  user: any;
  userRef: any;
  disableButton: boolean;
  disableButtonFree: boolean;
  scannerBool: boolean = false;
  isAdvSignUpActive: boolean = false;
  fyzz_free: boolean = false;
  scanText: string = '';
  scanTextFree: string = '';

  public login: boolean = false;
  payment_status: string;

  options: BarcodeScannerOptions;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translate: TranslateService,
    public alertCtrl: AlertController,
    public toast: ToastController,
    private afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    private barcodeScanner: BarcodeScanner,
    public nativeStorage: NativeStorage,
    public network: Network) {
      //this.loadData();
  }


  getLanguage() {
    this.nativeStorage.getItem('userSettings')
      .then(
        data => {
          this.userLang = data.language;
        },
        error => console.error(error)
      );
  }


  ionViewCanEnter(){
    this.getLanguage();

    this.advantageInfo = this.navParams.data;
    this.advInfoCollection = this.afs.collection<any>('Advantage', ref =>
      ref.where('Name', '==', this.advantageInfo.Name));
    this.advInfoObs = this.advInfoCollection.valueChanges();

    this.loadData();
    this.photoName1 = this.advantageInfo.Name +'1';
    this.photoName2 = this.advantageInfo.Name +'2';
    this.photoName3 = this.advantageInfo.Name +'3';
    this.getPhotoURL();

    this.advInfoCollection.ref.where('Name', '==', this.advantageInfo.Name).get().then((querySnapshot) =>{
      querySnapshot.forEach((doc) => {
        this.advAdvantage = doc.data().CodeA;
        this.advCode = doc.data().CodeVerif;
        this.advId = doc.id;
      });
    });
  }


  // ionViewWillEnter(){
  //   this.networkMonitor();
  // }


  ionViewDidEnter(){
    this.networkMonitor();
  }


  ionViewWillLeave(){
    this.networkConnection = false;
    //console.log("The connection is " + this.networkConnection);
    this.networkConnected.unsubscribe();
    this.networkDisconnected.unsubscribe();
  }


  networkMonitor(){
    this.translate.get(['NETWORK.CONNECTION', 'NETWORK.DISCONNECT'])
    .subscribe(res => {
      this.networkConnected = this.network.onDisconnect().subscribe(() => {
        //console.log('network was disconnected :-(');
        this.networkConnection = false;
        this.toast.create({
          message: res["NETWORK.DISCONNECT"],
          duration: 5000
        }).present();
      });

      this.networkDisconnected = this.network.onConnect().subscribe(() => {
        setTimeout(() => {
          //console.log('we got a connection, woohoo!');
          this.networkConnection = true;
          this.toast.create({
            message: res["NETWORK.CONNECTION"] + this.network.type,
            duration: 5000
          }).present();
        }, 3000);
      });
    });
  }



  loadData(){
    this.afAuth.authState.subscribe(async userf => {
      this.user = userf;
      if(this.user)  {
        this.login = true;
        var paymentRef = await firebase.firestore().collection('User').doc(this.user.uid).collection('Stripe').doc('Abonnement');
        paymentRef.get().then(async doc => {
          if(doc.exists){
            this.payment_status = await doc.data().fyzz_standard;
            console.log("PAYMENT STATUS IS: " + this.payment_status);
            this.networkConnection = true;
            //console.log("The network in loadData is " + this.networkConnection);

            if(this.payment_status != 'active'){
              this.disableButton = false;
            }
          } else {
            console.log('No such document!');
          }
        });

        this.userRef = await firebase.firestore().collection('User').doc(this.user.uid);
        this.userRef.get().then(async doc => {
          this.scannerBool = await doc.data().scannerAdvBool;
          this.fyzz_free = await doc.data().fyzz_free;
          console.log("FYZZ FREE : " + this.fyzz_free);
          this.isAdvSignUpActive = await doc.data().scannerAdvBoolSignup;
          if(this.scannerBool == true){
            this.disableButton = false;
            this.scanText = "scan_enable";
          }
          else if(this.scannerBool == false){
            this.disableButton = true;
            this.scanText = "scan_disable";
          }
          if (this.isAdvSignUpActive == true ){
            this.disableButtonFree = false;
            this.scanTextFree = "scan_enable";
          }
          else if (this.isAdvSignUpActive == false ){
            this.disableButtonFree = true;
            this.scanTextFree = "scan_disable";
          }
        });
      } else {
        this.login = false;
      }
    });
  }


  openSignUp(){
    this.navCtrl.push('SignupPage');
  }


  openAboPage(){
      this.navCtrl.push('AbonnementPage');
  }


  openSettings(){
    this.navCtrl.push('SettingsPage');
  }


  openHome(){
    this.navCtrl.push('HomePage');
  }


  openContact(){
    this.navCtrl.push('ContactPage');
  }

  toggleHours() {
    if (this.show_hours == false) {
      this.show_hours = true;
    }
    else {
      this.show_hours = false;
    }
  }


  openScanner(){
    console.log("OPENSCANNER")

    this.translate.get(['ABR.SCANQR','ABR.CANCEL','ABR.VERIFY','ABR.VERIFYTXTADV','ABR.VERIFYCODE','ABR.VERIFYENTERCODE','ABR.VERIFYCANCEL','ABR.VERIFYCANCELTXT','ABR.ADVVALIDATED','ABR.ADVVALIDATEDTXT','ABR.SCANCANCEL','ABR.SCANERROR','ABR.SCANERRORTXT','ABR.SCANACTIVATION','ABR.SCANACTIVATIONTXT','ABR.SCANSUBSCRIBE','ABR.SCANSUBSCRIBETXT', 'NETWORK.TURNONCONNECTION'])
    .subscribe(res => {
      this.options = {
        prompt: res["ABR.SCANQR"],
        resultDisplayDuration: 0,
        disableSuccessBeep: false
      }
      this.barcodeScanner.scan(this.options).then(barcodeData => {
        if(this.user){
          if(this.payment_status == 'active' && this.networkConnection == true){
            console.log("HELLO DEEPSHITCODE");
            let qrCode = barcodeData.text.toString();
              if(this.advAdvantage == qrCode){
                let alert = this.alertCtrl.create({
                  title: res["ABR.VERIFY"],
                  enableBackdropDismiss: false,
                  subTitle: res["ABR.VERIFYTXTADV"],
                  inputs: [
                    {
                      name:"Code",
                      placeholder:res["ABR.VERIFYENTERCODE"],
                      type: 'password'
                    }
                  ],
                  buttons: [
                    {
                      text: res["ABR.CANCEL"],
                      role: 'cancel',
                      handler: data =>{
                        alert.dismiss('annule');
                        return false;
                      }
                    },
                    {
                      text: 'OK',
                      handler: data =>{
                        if(data.Code == this.advCode){
                          alert.dismiss(true);
                          return false;
                        }else{
                          alert.dismiss(false);
                          return false;
                        }
                      }
                    }
                  ]
                });
                  alert.present();
                  alert.onDidDismiss((data)=>{
                    if(data==true && this.networkConnection == true){
                      let alert = this.alertCtrl.create({
                        title: res["ABR.ADVVALIDATED"],
                        subTitle: res["ABR.ADVVALIDATEDTXT"],
                        buttons: ['OK']
                      });
                      alert.present();
                      //Increment the user counter
                      var transaction = firebase.firestore()
                      .runTransaction(t => {
                        return t.get(this.userRef).then(doc => {
                          var newAdv = doc.data().counterAdv + 1;
                          t.update(this.userRef, { "counterAdv": newAdv, "scannerAdvBool": false });
                          this.disableButton = true;
                          });
                        })
                      // increment the advantage counter
                        var advRef = firebase.firestore().collection("Advantage").doc(this.advId);
                        var transaction = firebase.firestore()
                        .runTransaction(t => {
                          return t.get(advRef).then(doc => {
                            var newAdvScan = doc.data().CounterAdv + 1;
                            t.update(advRef, { "CounterAdv": newAdvScan });
                          });
                        })
                      }
                    else{
                      if(data == false){
                        let alert = this.alertCtrl.create({
                          title: res["ABR.VERIFYCANCEL"],
                          subTitle: res["ABR.VERIFYCANCELTXT"],
                          buttons: ['OK']
                        });
                        alert.present();
                      } else if(this.networkConnection == false){
                        let alert = this.alertCtrl.create({
                          title: "ERROR 404",
                          subTitle: res["NETWORK.TURNONCONNECTION"],
                          buttons: ['OK']
                        });
                        alert.present();
                      }
                      else {
                        if('cancel'){
                          let alert = this.alertCtrl.create({
                            title: res["ABR.VERIFYCANCEL"],
                            buttons: ['OK']
                          });
                          alert.present();
                        }
                      }
                    }
                  })
                } else {
                  if(barcodeData.cancelled == true){
                    let alert = this.alertCtrl.create({
                      title: res["ABR.SCANCANCEL"],
                      buttons: ['OK']
                    });
                    alert.present();
                } else{
                  let alert = this.alertCtrl.create({
                    title: res["ABR.SCANERROR"],
                    subTitle: res["ABR.SCANERRORTXT"],
                    buttons: ['OK']
                  });
                  alert.present();
                }
              }
              console.log('Barcode data', barcodeData);
          }
          // else if (this.isAdvSignUpActive == true && this.networkConnection == true){
          //   let qrCode = barcodeData.text.toString();
          //     if(this.advAdvantage == qrCode){
          //       let alert = this.alertCtrl.create({
          //         title: res["ABR.VERIFY"],
          //         enableBackdropDismiss: false,
          //         subTitle: res["ABR.VERIFYTXTADV"],
          //         inputs: [
          //           {
          //             name:"Code",
          //             placeholder:res["ABR.VERIFYENTERCODE"],
          //             type: 'password'
          //           }
          //         ],
          //         buttons: [
          //           {
          //             text: res["ABR.CANCEL"],
          //             role: 'cancel',
          //             handler: data =>{
          //               alert.dismiss('annule');
          //               return false;
          //             }
          //           },
          //           {
          //             text: 'OK',
          //             handler: data =>{
          //               if(data.Code == this.advCode){
          //                 alert.dismiss(true);
          //                 return false;
          //               }else{
          //                 alert.dismiss(false);
          //                 return false;
          //               }
          //             }
          //           }
          //         ]
          //       });
          //         alert.present();
          //         alert.onDidDismiss((data)=>{
          //           if(data==true && this.networkConnection == true){
          //             let alert = this.alertCtrl.create({
          //               title: res["ABR.ADVVALIDATED"],
          //               subTitle: res["ABR.ADVVALIDATEDTXT"],
          //               buttons: ['OK']
          //             });
          //             alert.present();
          //             //Increment the user counter
          //             var transaction = firebase.firestore()
          //             .runTransaction(t => {
          //               return t.get(this.userRef).then(doc => {
          //                 var newAdv = doc.data().counterAdv + 1;
          //                 t.update(this.userRef, { "counterAdv": newAdv, "scannerAdvBoolSignup": false });
          //                 this.disableButton = true;
          //                 });
          //               })
          //             // increment the advantage counter
          //               var advRef = firebase.firestore().collection("Advantage").doc(this.advId);
          //               var transaction = firebase.firestore()
          //               .runTransaction(t => {
          //                 return t.get(advRef).then(doc => {
          //                   var newAdvScan = doc.data().CounterAdv + 1;
          //                   t.update(advRef, { "CounterAdv": newAdvScan });
          //                 });
          //               })
          //             }
          //           else{
          //             if(data == false){
          //               let alert = this.alertCtrl.create({
          //                 title: res["ABR.VERIFYCANCEL"],
          //                 subTitle: res["ABR.VERIFYCANCELTXT"],
          //                 buttons: ['OK']
          //               });
          //               alert.present();
          //             } else if(this.networkConnection == false){
          //               let alert = this.alertCtrl.create({
          //                 title: "ERROR 404",
          //                 subTitle: res["NETWORK.TURNONCONNECTION"],
          //                 buttons: ['OK']
          //               });
          //               alert.present();
          //             }
          //             else {
          //               if('cancel'){
          //                 let alert = this.alertCtrl.create({
          //                   title: res["ABR.VERIFYCANCEL"],
          //                   buttons: ['OK']
          //                 });
          //                 alert.present();
          //               }
          //             }
          //           }
          //         })
          //       } else {
          //         if(barcodeData.cancelled == true){
          //           let alert = this.alertCtrl.create({
          //             title: res["ABR.SCANCANCEL"],
          //             buttons: ['OK']
          //           });
          //           alert.present();
          //       } else{
          //         let alert = this.alertCtrl.create({
          //           title: res["ABR.SCANERROR"],
          //           subTitle: res["ABR.SCANERRORTXT"],
          //           buttons: ['OK']
          //         });
          //         alert.present();
          //       }
          //     }
          //     console.log('Barcode data', barcodeData);
          // }
          else if (this.networkConnection == false){
            let alert = this.alertCtrl.create({
              title: "ERROR 404",
              subTitle: res["NETWORK.TURNONCONNECTION"],
              buttons: ['OK']
            });
            alert.present();
          }
          // else {
          //   let alert = this.alertCtrl.create({
          //     title: res["ABR.SCANACTIVATION"],
          //     subTitle: res["ABR.SCANACTIVATIONTXT"],
          //     buttons: ['OK']
          //   });
          //   alert.present()
          // }
        } else {
          let alert = this.alertCtrl.create({
            title: res["ABR.SCANSUBSCRIBE"],
            subTitle: res["ABR.SCANSUBSCRIBETXT"],
            buttons: ['OK']
          });
          alert.present();
        }
      }).catch(err => {
        console.log('Error', err);
      });
    });
  }



  openFreeScanner(){
    console.log("OPENFREESCANNER");
    this.translate.get(['ABR.SCANQR','ABR.CANCEL','ABR.VERIFY','ABR.VERIFYTXTADV','ABR.VERIFYCODE','ABR.VERIFYENTERCODE','ABR.VERIFYCANCEL','ABR.VERIFYCANCELTXT','ABR.ADVVALIDATED','ABR.ADVVALIDATEDTXT','ABR.SCANCANCEL','ABR.SCANERROR','ABR.SCANERRORTXT','ABR.SCANACTIVATION','ABR.SCANACTIVATIONTXT','ABR.SCANSUBSCRIBE','ABR.SCANSUBSCRIBETXT', 'NETWORK.TURNONCONNECTION'])
    .subscribe(res => {
      this.options = {
        prompt: res["ABR.SCANQR"],
        resultDisplayDuration: 0,
        disableSuccessBeep: false
      }
      this.barcodeScanner.scan(this.options).then(barcodeData => {
        if(barcodeData.cancelled == true){
          let alert = this.alertCtrl.create({
            title: res["ABR.SCANCANCEL"],
            buttons: ['OK']
          });
          alert.present();
          return;
        }
        console.log("HELLO 1")
        if(this.user){
          console.log("HELLO 2")
          if(this.fyzz_free == true){
            console.log("HELLO DEEPSHITCODE");
            let qrCode = barcodeData.text.toString();
            console.log("QRCODE IS : " + qrCode)
              if(this.advAdvantage == qrCode){
                let alert = this.alertCtrl.create({
                  title: res["ABR.VERIFY"],
                  enableBackdropDismiss: false,
                  subTitle: res["ABR.VERIFYTXTADV"],
                  inputs: [
                    {
                      name:"Code",
                      placeholder:res["ABR.VERIFYENTERCODE"],
                      type: 'password'
                    }
                  ],
                  buttons: [
                    {
                      text: res["ABR.CANCEL"],
                      role: 'cancel',
                      handler: data =>{
                        alert.dismiss('annule');
                        console.log("CANCEL BTN PUSHED");
                        return false;
                      }
                    },
                    {
                      text: 'OK',
                      handler: data =>{
                        if(data.Code == this.advCode){
                          alert.dismiss(true);
                          console.log("The data.Code is: " + data.Code);
                          console.log("This.advCode is: " + this.advCode);
                          console.log("THE DATA HANDLER IS OK");
                          return false;
                        }else{
                          alert.dismiss(false);
                          console.log("THE DATA HANDLER IS NOT OK");
                          console.log("The data.Code is: " + data.Code);
                          console.log("This.advCode is: " + this.advCode);
                          return false;
                        }
                      }
                    }
                  ]
                });
                alert.present();
                console.log("ONDIDDISMISS LEVEL");
                alert.onDidDismiss((data)=>{
                  console.log("The Data is: " + data);
                  if(data==true){
                    let alert = this.alertCtrl.create({
                      title: res["ABR.ADVVALIDATED"],
                      subTitle: res["ABR.ADVVALIDATEDTXT"],
                      buttons: ['OK']
                    });
                    alert.present();
                    //Increment the user counter
                    var transaction = firebase.firestore()
                    .runTransaction(t => {
                      return t.get(this.userRef).then(doc => {
                        var newAdv = doc.data().counterAdv + 1;
                        t.update(this.userRef, { "counterAdv": newAdv, "scannerAdvBoolSignup": false });
                        this.disableButtonFree = true;
                        });
                      })
                    // increment the advantage counter
                      var advRef = firebase.firestore().collection("Advantage").doc(this.advId);
                      var transaction = firebase.firestore()
                      .runTransaction(t => {
                        return t.get(advRef).then(doc => {
                          var newAdvScan = doc.data().CounterAdv + 1;
                          t.update(advRef, { "CounterAdv": newAdvScan });
                        });
                      })
                    }
                  else{
                    if(data == false){
                      let alert = this.alertCtrl.create({
                        title: res["ABR.VERIFYCANCEL"],
                        subTitle: res["ABR.VERIFYCANCELTXT"],
                        buttons: ['OK']
                      });
                      alert.present();
                    }
                    // else if(this.networkConnection == false){
                    //   let alert = this.alertCtrl.create({
                    //     title: "ERROR 404",
                    //     subTitle: res["NETWORK.TURNONCONNECTION"],
                    //     buttons: ['OK']
                    //   });
                    //   alert.present();
                    // }
                    else {
                      if('cancel'){
                        let alert = this.alertCtrl.create({
                          title: res["ABR.VERIFYCANCEL"],
                          buttons: ['OK']
                        });
                        alert.present();
                      }
                    }
                  }
                })
              } else {
                let alert = this.alertCtrl.create({
                  title: res["ABR.SCANERROR"],
                  subTitle: res["ABR.SCANERRORTXT"],
                  buttons: ['OK']
                });
                alert.present();
              }
            //}
            console.log('Barcode data', barcodeData);
          }
          // else if (this.networkConnection == false){
          //   let alert = this.alertCtrl.create({
          //     title: "ERROR 404",
          //     subTitle: res["NETWORK.TURNONCONNECTION"],
          //     buttons: ['OK']
          //   });
          //   alert.present();
          // }
          else {
            let alert = this.alertCtrl.create({
              title: res["ABR.SCANACTIVATION"],
              subTitle: res["ABR.SCANACTIVATIONTXT"],
              buttons: ['OK']
            });
            alert.present()
          }
        } else {
          let alert = this.alertCtrl.create({
            title: res["ABR.SCANSUBSCRIBE"],
            subTitle: res["ABR.SCANSUBSCRIBETXT"],
            buttons: ['OK']
          });
          alert.present();
        }
      }).catch(err => {
        console.log('Error in the QR Scanning', err);
      });
    });
  }



  getPhotoURL(){
    firebase.storage().ref().child('Avantage/'+this.photoName1+'.jpg').getDownloadURL()
    .then((url) =>{
      this.photo1 = url;
    })
    firebase.storage().ref().child('Avantage/'+this.photoName2+'.jpg').getDownloadURL()
    .then((url) =>{
      this.photo2 = url;
    })
    firebase.storage().ref().child('Avantage/'+this.photoName3+'.jpg').getDownloadURL()
    .then((url) =>{
      this.photo3 = url;
    })
  }
}
