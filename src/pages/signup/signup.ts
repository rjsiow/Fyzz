import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, LoadingController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import moment from 'moment';
import { AuthService } from '../../services/auth.service';
import {AngularFireAuth} from 'angularfire2/auth';
import firebase from 'firebase/app';
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})

export class SignupPage {
  signupError: string;
  signupForm: FormGroup;
  today;
  showPwd: boolean = false;
  pwdType: string = "password";
  pwdIcon: string = "eye-off";
  loading:any;

  constructor(
    public navCtrl: NavController,
    public translate: TranslateService,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public afAuth: AngularFireAuth,
    private auth: AuthService,
    fb: FormBuilder) {
      this.today = this.minimumAge();
      this.signupForm = fb.group({
			     email: ['', Validators.compose([Validators.required, Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')])],
           emailConfirm: ['', Validators.required],
			     password: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')])],
			     city: ['', Validators.compose([Validators.required])],
			     firstname: ['', Validators.compose([Validators.required])],
			     lastname: ['', Validators.compose([Validators.required])],
           birthdate: ['', Validators.compose([Validators.required])],
			     postalCode: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
           //phoneNumber: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
           checkbox1: ['false',Validators.requiredTrue],
           checkbox2: ['false',Validators.requiredTrue],
           radioSexe: ['']
		}, {validator: this.matchingEmails('email', 'emailConfirm')});
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }


  showHidePassword()
  {
    this.pwdType = this.pwdType === 'text' ? 'password' : 'text';
    this.pwdIcon = this.pwdIcon === 'eye-off' ? 'eye' : 'eye-off';
  }


  matchingEmails(emailKey: string, emailConfirmKey: string) {
    return (group: FormGroup): {[key: string]: any} => {
      let email = group.controls[emailKey];
      let emailConfirm = group.controls[emailConfirmKey];

      if (email.value !== emailConfirm.value) {
        return {
          mismatchedEmails: true
        };
      }
    }
  }


  showLoadingAlert(){
    this.translate.get(['SIGNUP.CREATEACOUNT','SIGNUP.SUCCESS','SIGNUP.SUCCESSTXT','SIGNUP.ERROR','SIGNUP.ERRORTXT'])
    .subscribe(res => {
      this.loading = this.loadingCtrl.create({
        content: res["SIGNUP.CREATEACOUNT"],
        duration: 5000,
      });
      this.loading.present();
      this.loading.onDidDismiss(() => {
        //this.afAuth.authState.subscribe(user => {
        var user = firebase.auth().currentUser;
          if (user) {
            let alert = this.alertCtrl.create({
              title: res["SIGNUP.SUCCESS"],
              enableBackdropDismiss: false,
              subTitle: res["SIGNUP.SUCCESSTXT"],
              buttons: [
                {
                  text: 'OK',
                  handler: () =>{
                    this.navCtrl.setRoot('MapPage');
                  }
                }
              ]
            });
            alert.present();
          }else{
            let alert = this.alertCtrl.create({
              title: res["SIGNUP.ERROR"],
              enableBackdropDismiss: false,
              subTitle: res["SIGNUP.ERRORTXT"],
              buttons: [
                {
                  text: 'OK',
                }
              ]
            });
            alert.present();
          }
        //})
      })
    });
  }


  minimumAge(){
    var today = moment();
    var maxValue = today.subtract(16, "year").format('YYYY-MM-DD');
    console.log(maxValue);
    return maxValue;
  }


  signup() {
		let data = this.signupForm.value;
		let credentials = {
      firstname: data.firstname,
      lastname: data.lastname,
			email: data.email,
      password: data.password,
      postalCode: data.postalCode,
      city: data.city,
      birthdate: data.birthdate,
      sexe: data.radioSexe
		};
		this.auth.signUp(credentials).then(
			() => this.afAuth.authState.subscribe(async user => {
          if (user) {
            user.sendEmailVerification();
          }
        }),
			error => this.signupError = error.message
    );
    this.showLoadingAlert();
  }
}
