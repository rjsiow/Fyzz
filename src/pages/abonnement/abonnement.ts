import { Component } from '@angular/core';
import { ActionSheetController, IonicPage, NavController, AlertController, LoadingController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import firebase from 'firebase/app';
import { Platform } from 'ionic-angular';
import { environment } from '../../app/credentials';
import { PaymentService } from '../../services/payment.service';

/**
 * Generated class for the AbonnementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-abonnement',
  templateUrl: 'abonnement.html',
})
export class AbonnementPage {

  isUserVerified: boolean;
  emailVerif: boolean;
  loading: any;
  userEmail: string;
  userId : string;
  handler: any;  // stripe handler
  aboType : string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translate: TranslateService,
    public platform: Platform,
    public afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private afs: AngularFirestore,
    private pmt: PaymentService,
    public actionSheetController: ActionSheetController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AbonnementPage');
  }

  openSettings(){
      this.navCtrl.push('SettingsPage');
  }

  ionViewCanEnter(){
    this.loadData();
    // this.configHandler1290();
    // this.configHandler3490();
    //this.configHandler5990();
  }

  loadData(){
    this.afAuth.authState.subscribe(async user => {
      if (user) {
        this.isUserVerified = await user.emailVerified;
        if(this.isUserVerified == false){
          await user.reload();
          await user.getIdToken(true);
          this.isUserVerified = await user.emailVerified;
        }

        console.log("Is the user verified? " + this.isUserVerified);
        var docRef = await firebase.firestore().collection('User').doc(user.uid);
        docRef.get().then(async doc => {
          if (doc.exists) {
            var data = await doc.data();
            //console.log("The User Data is: ", this.userData);
            this.userEmail = doc.data().email;
            this.userId = user.uid;
            this.emailVerif = doc.data().emailVerif;
            console.log(this.userId);
          } else {
            console.log('No such document!');
          }
        })
        .catch(function(error) {
          console.log('Error getting document:', error);
        });
      }
    });
  }


//------------abo 12.90-----------------------------------------

  async stripeSubscribe1290(){
    this.aboType = "1290";
    this.configHandler();
    this.translate.get(['ABO.CURRENCY1290','ABO.EMAILVALIDATE','ABO.EMAILVALIDATETXT'])
    .subscribe(res => {
      if(this.isUserVerified == true || this.emailVerif == true){
        this.handler.open({
          name: 'FYZZ',
          description: "12,90 " + res["ABO.CURRENCY1290"],
          email: this.userEmail,
          amount: 1290,
          currency: 'CHF'
        });
        this.platform.backButton.subscribe(() => {
          // close alert
          if(this.handler){
            try {
              this.handler.close();
            } catch (error) {
            }
          }
        });
      }else{
        let alert = this.alertCtrl.create({
          title: res["ABO.EMAILVALIDATE"],
          subTitle: res["ABO.EMAILVALIDATETXT"],
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                console.log('Confirm Okay');
              }
            }
          ]
        });
        alert.present();
      }
    });
  }

  // private configHandler1290() {
  //   this.handler = StripeCheckout.configure({
  //     key: environment.stripeKey,
  //     image: 'assets/imgs/logo_checkout.png',
  //     locale: 'auto',
  //     allowRememberMe: false,
  //     token: token => {
  //       this.pmt.processPayment1290(token);
  //       this.showLoadingAlert();
  //     }
  //   });
  // }

//----------------------------------------------------------------

//------------abo 34.90-----------------------------------------

async stripeSubscribe3490(){
  this.aboType = "3490";
  this.configHandler();
  this.translate.get(['ABO.CURRENCY3490','ABO.EMAILVALIDATE','ABO.EMAILVALIDATETXT'])
  .subscribe(res => {
    if(this.isUserVerified == true || this.emailVerif == true){
      this.handler.open({
        name: 'FYZZ',
        description: "34,90 " + res["ABO.CURRENCY3490"],
        email: this.userEmail,
        amount: 3490,
        currency: 'CHF'
      });
      this.platform.backButton.subscribe(() => {
        // close alert
        if(this.handler){
          try {
            this.handler.close();
          } catch (error) {
          }
        }
      });
    }else{
      let alert = this.alertCtrl.create({
        title: res["ABO.EMAILVALIDATE"],
        subTitle: res["ABO.EMAILVALIDATETXT"],
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              console.log('Confirm Okay');
            }
          }
        ]
      });
      alert.present();
    }
  });
}
//
// private configHandler3490() {
//   this.handler = StripeCheckout.configure({
//     key: environment.stripeKey,
//     image: 'assets/imgs/logo_checkout.png',
//     locale: 'auto',
//     allowRememberMe: false,
//     token: token => {
//       this.pmt.processPayment3490(token);
//       this.showLoadingAlert();
//     }
//   });
// }

//----------------------------------------------------------------
//------------abo 59.90-----------------------------------------

async stripeSubscribe5990(){
  this.aboType = "5990";
  this.configHandler();
  this.translate.get(['ABO.CURRENCY5990','ABO.EMAILVALIDATE','ABO.EMAILVALIDATETXT'])
  .subscribe(res => {
    if(this.isUserVerified == true || this.emailVerif == true){
      this.handler.open({
        name: 'FYZZ',
        description: "59,90 " + res["ABO.CURRENCY5990"],
        email: this.userEmail,
        amount: 5990,
        currency: 'CHF'
      });
      this.platform.backButton.subscribe(() => {
        // close alert
        if(this.handler){
          try {
            this.handler.close();
          } catch (error) {
          }
        }
      });
    }else{
      let alert = this.alertCtrl.create({
        title: res["ABO.EMAILVALIDATE"],
        subTitle: res["ABO.EMAILVALIDATETXT"],
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              console.log('Confirm Okay');
            }
          }
        ]
      });
      alert.present();
    }
  });
}


//--------------------Config Handler----------------------------

private configHandler() {
  if(this.aboType == "1290"){
    this.handler = StripeCheckout.configure({
      key: environment.stripeKey,
      image: 'assets/imgs/logo_checkout.png',
      locale: 'auto',
      allowRememberMe: false,
      token: token => {
        this.pmt.processPayment1290(token);
        this.showLoadingAlert();
      }
    });
  }
  else if (this.aboType == "3490"){
    this.handler = StripeCheckout.configure({
      key: environment.stripeKey,
      image: 'assets/imgs/logo_checkout.png',
      locale: 'auto',
      allowRememberMe: false,
      token: token => {
        this.pmt.processPayment3490(token);
        this.showLoadingAlert();
      }
    });
  } else if (this.aboType == "5990"){
    this.handler = StripeCheckout.configure({
      key: environment.stripeKey,
      image: 'assets/imgs/logo_checkout.png',
      locale: 'auto',
      allowRememberMe: false,
      token: token => {
        this.pmt.processPayment5990(token);
        this.showLoadingAlert();
      }
    });
  }

}


//--------------------Alert----------------------------

  showLoadingAlert(){
    this.translate.get(['ABO.SUBSCRIPTIONACTIVATION','ABO.SUCCESS','ABO.SUCCESSTXT','ABO.ERROR','ABO.ERRORTXT'])
    .subscribe(res => {
      this.loading = this.loadingCtrl.create({
        content: res["ABO.SUBSCRIPTIONACTIVATION"],
        duration: 20000
      });

      this.loading.present();
      this.loading.onDidDismiss(()=>{
        this.afAuth.authState.subscribe(user => {
          if (user) {
            var statusDoc = firebase.firestore().collection('User').doc(user.uid)
            .collection('Stripe').doc('Abonnement')
            .get().then( doc => {
              if (doc.exists) {
                let alert = this.alertCtrl.create({
                  title: res["ABO.SUCCESS"],
                  subTitle: res["ABO.SUCCESSTXT"],
                  buttons: [
                    {
                      text: 'OK',
                      handler: () =>{
                        this.navCtrl.setRoot('MapPage');
                        this.showTutorialAlert();
                      }
                    }
                  ]
                });
                alert.present();
              } else {
                let alert = this.alertCtrl.create({
                  title: res["ABO.ERROR"],
                  subTitle: res["ABO.ERRORTXT"],
                  buttons: [
                    {
                      text: 'OK',
                    }
                  ]
                });
                alert.present();
              }
            })
          }
        })
      })
    });
  }

  showTutorialAlert(){
    this.translate.get(['ABO.TUTORIAL', 'ABO.STEPS'])
    .subscribe(res => {
      let alert = this.alertCtrl.create({
        title: res["ABO.TUTORIAL"],
        enableBackdropDismiss: false,
        message: "<div class='textNotif'>" + res["ABO.STEPS"] + "</div>",
        buttons: [
          {
            text: 'OK',
            handler: () =>{
            }
          }
        ]
      });
      alert.present();
    });
  }
}
