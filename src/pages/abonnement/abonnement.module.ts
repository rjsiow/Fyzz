import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AbonnementPage } from './abonnement';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

@NgModule({
  declarations: [
    AbonnementPage,
  ],
  imports: [
    HttpClientModule,
    IonicPageModule.forChild(AbonnementPage),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    AbonnementPage
  ]
})
export class AbonnementPageModule {}
