import { Component, ViewChild, ElementRef} from '@angular/core';
import { IonicPage, NavController, ActionSheetController, AlertController, ToastController, NavParams, LoadingController, Platform, Slides } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import leaflet from 'leaflet';
//import { NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { MarkersProvider } from "../../providers/markers/markers";
//import { Observable } from 'rxjs-compat';
import * as $ from 'jquery';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Network } from '@ionic-native/network';
import { Subscription} from 'rxjs/Subscription';


@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  array: Array<any> = [];
  sortedArray: Array<any> = [];
  // array2: Array<any> = [];
  searchTerm: string = '';
  loading: any;

  drinkEnable: boolean;
  bannerText: string = '';
  isDrinkActive: boolean;
  isAdvActive: boolean;
  isAdvSignUpActive: boolean;
  fyzz_free: boolean;

  public login: boolean = false;
  user: any;
  data: any;
  // count: number;
  // price: string;

  @ViewChild('map') mapContainer: ElementRef;
  map: any;
  lat: number;
  lng: number;

  center: leaflet.PointTuple = [46.617799, 7.057241];
  isLocationEnabled: boolean;
  userLocation: [0, 0];
  @ViewChild('slides') slides: Slides;
  show_explore = false;
  show_price = false;

  //NETWORK CONNECTION
  networkConnection: boolean;
  networkConnected: Subscription;
  networkDisconnected: Subscription;

  //SCANNING VARIABLES
  options: BarcodeScannerOptions;
  userRef: any;

  constructor(
    public navCtrl: NavController,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public navParams: NavParams,
    private platform: Platform,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    private afs: AngularFirestore,
    private markersProvider: MarkersProvider,
    public afAuth: AngularFireAuth,
    private barcodeScanner: BarcodeScanner,
    public network: Network) {
      this.loadBanner();
  }

  ionViewDidLoad() {
    this.presentLoading();
    this.loadmap();
  }


  ionViewWillEnter(){
    this.map.invalidateSize();
  }


  ionViewCanEnter(){
    this.loadBanner();
  }


  // ionViewDidEnter(){
  //   this.networkMonitor();
  // }


  loadBanner() {
    this.afAuth.authState.subscribe(async user => {
      this.user = user;
      if(user){
        console.log("USER IS LOGGED!\n");
        this.userRef = await firebase.firestore().collection('User').doc(user.uid)
        var snapShotScan = await firebase.firestore().collection('User').doc(user.uid).get();
        this.isDrinkActive = snapShotScan.data().scannerDrinkBool;
        this.isAdvActive = snapShotScan.data().scannerAdvBool;
        this.isAdvSignUpActive = snapShotScan.data().scannerAdvBoolSignup;
        this.fyzz_free = snapShotScan.data().fyzz_free;

        var snapShotPayment = await firebase.firestore()
            .collection('User')
            .doc(user.uid)
            .collection('Stripe')
            .doc('Abonnement').get().then(async docSnapshot =>{
              if (docSnapshot.exists){
                this.data = await docSnapshot.data().fyzz_standard;
                console.log("The payment is : ", this.data);

                if(this.data == 'active' && this.isDrinkActive == true){
                  document.getElementById('drink_banner_yes').style.background = 'linear-gradient(to left, #7d7777, #03bfbf)';
                } else {
                  document.getElementById('drink_banner_yes').style.background = 'linear-gradient(to left, #7d7777, #b3b3b3)';
                }

                if(this.data == 'active' && this.isAdvActive == true){
                  document.getElementById('adv_banner_yes').style.background = 'linear-gradient(to left, #f24343, #7d7777)';
                } else {
                  document.getElementById('adv_banner_yes').style.background = 'linear-gradient(to left, #b3b3b3, #7d7777)';
                }

              } else {
                console.log('No such document');
                console.log("THE DATA IS: " + this.data);
                if(this.isAdvSignUpActive == true){
                  console.log("SignUp banner is active")
                  document.getElementById('signup_banner').style.background = 'linear-gradient(to left, #f24343, #f27c7d)';
                } else if (this.isAdvSignUpActive == false){
                  console.log("SignUp banner is NOT active")
                  document.getElementById('signup_banner').style.background = '#8b8c7f';
                }
              }
            });





        console.log("SignUp banner is: " + this.isAdvSignUpActive)


      //
      //   if(this.data == null){
      //     let count = this.afs.collection(`/User`)
      //                .snapshotChanges()
      //                .subscribe(c => {
      //                  this.count = c.length;
      //                  if(this.count > 100 && this.count <= 500){
      //                    this.price = 'CHF 14.90'
      //                    console.log("The price is " + this.price)
      //                  } else if(this.count > 500){
      //                    this.price = 'CHF 19.90'
      //                  }
      //                });
      //   }
      // }
      // else {
      //   let count = this.afs.collection(`/User`)
      //              .snapshotChanges()
      //              .subscribe(c => {
      //                this.count = c.length;
      //                if(this.count > 100 && this.count <= 500){
      //                  this.price = 'CHF 14.90'
      //                  console.log("The price is " + this.price)
      //                } else if(this.count > 500){
      //                  this.price = 'CHF 19.90'
      //                }
      //              });

      //console.log("There's no user!");
      }
    });
  }


  presentLoading() {
    this.loading = this.loadingCtrl.create({
      duration: 2500,
      dismissOnPageChange: true,
      cssClass: 'my-loading-class'
    });
    this.loading.present();
  }


  openSettings(){
    this.navCtrl.push('SettingsPage');
  }

  openTutorial(){
    this.navCtrl.push('TutorialPage');
  }

  openContact(){
    this.navCtrl.push('ContactPage');
  }

  openSignUp(){
    this.navCtrl.push('SignupPage');
  }

  openAboPage(){
      this.navCtrl.push('AbonnementPage');
  }

  togglePrice() {
    if (this.show_price == false) {
      this.show_price = true;
    }
    else {
      this.show_price = false;
    }
  }

  presentPrice() {
    this.translate.get(['MAP.FREE','MAP.ACTIONSHEET1290','MAP.ACTIONSHEET3490','MAP.ACTIONSHEET5990'])
    .subscribe(res => {
      const actionSheet = this.actionSheetCtrl.create({
        buttons: [
          {
            text: res["MAP.FREE"],
            handler: () => {
              this.openSignUp();
            }
          },
          {
            text: res["MAP.ACTIONSHEET1290"],
            handler: () => {
              this.openSignUp();
            }
          },
          {
            text: res["MAP.ACTIONSHEET3490"],
            handler: () => {
              this.openSignUp();
            }
          },
          {
            text: res["MAP.ACTIONSHEET5990"],
            handler: () => {
              this.openSignUp();
            }
          },
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    })
  }
  // opens the page where new users can register
  // or current user can see their profile, benefits etc.
  openHome(){
    //check if user is already logged-in, and redirect him accordingly.
    var user = firebase.auth().currentUser;

    if(user)  {
      this.navCtrl.push('ContactPage');

    }else{
      this.navCtrl.push('ConnexionPage');
    }
  }

  openPage(){
    let index = this.slides.getActiveIndex();
    console.log("The index for opening page is: " + index)
    console.log("And the type is: " + this.sortedArray[index].Type);
    if (this.sortedArray[index].Type == 'BarPage') {
      this.openBarPage(this.sortedArray[index])
    }
    else if (this.sortedArray[index].Type == 'RestaurantPage') {
      this.openRestoPage(this.sortedArray[index])
    }
    else if (this.sortedArray[index].Type == 'AdvantagePage') {
      this.openAdvantagePage(this.sortedArray[index])
    }
  }

  openEventsPage(){
    this.navCtrl.push('EventsPage');
  }

  openBarPage(barInfo){
    this.navCtrl.push('BarPage', barInfo);
  }


  openRestoPage(restoInfo){
    this.navCtrl.push('RestaurantPage', restoInfo);
  }


  openAdvantagePage(advantageInfo){
    this.navCtrl.push('AdvantagePage', advantageInfo);
  }


  openListeBar(){
    this.navCtrl.push('ListeBarPage');
  }


  openListeRestaurant(){
    this.navCtrl.push('ListeRestaurantPage');
  }


  openListeAdvantage(){
    this.navCtrl.push('ListeAdvantagePage');
  }

  openSearch(){
    this.sortTable()
    this.navCtrl.push('SearchPage', this.sortedArray);
  }


  // filterItems(searchTerm){
  //   return this.array.filter((item) => {
  //       return item.Name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
  //   });
  // }
  //
  //
  // setFilteredItems() {
  //   this.array2 = this.filterItems(this.searchTerm);
  //   console.log(this.array2);
  // }

  toggleExplore() {
    if (this.show_explore == false) {
      this.show_explore = true;
      this.sortTable();
    }
    else {
      this.show_explore = false;
    }
  }


  slideChanged(){
    // Get the index of the active slide:
    let index = this.slides.getActiveIndex(); // getActiveIndex is considered undefined
    console.log("Current index is" + index);
    this.shopRecenter(this.sortedArray[index].Coord);
  }


  recenter(){
    if(this.isLocationEnabled == true){
      this.map.setView([this.lat, this.lng], 13);
    } else if (this.isLocationEnabled == false){      // centre sur Bulle
      this.map.setView(this.center, 13);
    }
  }

  shopRecenter(coord){
    coord = [coord[0] - 0.000355, coord[1]];
    this.map.setView(coord, 20);
  }


  loadmap() {
    this.map = leaflet.map("map", {attributionControl: false}).fitWorld();
    this.map.zoomControl.remove();
    leaflet.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}{r}.png', {
      attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      subdomain: 'abcd',
      minZoom: 7,
      maxZoom: 19
    }).addTo(this.map);
    this.map.locate({
      setView: true,
      maxZoom: 10
    }).on('locationfound', (e) => {
      this.lat = e.latitude;
      this.lng = e.longitude;
      this.isLocationEnabled = true;
      this.userLocation = [e.latitude, e.longitude];
      console.log(this.userLocation);
      let markerGroup = leaflet.featureGroup();
      let marker: any = leaflet.marker([e.latitude, e.longitude]);
      markerGroup.addLayer(marker);
      this.map.addLayer(markerGroup);
      this.loadMarkers();
    }).on('locationerror', (e) => {
      this.isLocationEnabled = false;
      console.log('error on getting location');
      this.map.setView(this.center, 15);
      this.loadMarkers();
    });
    //this.loadMarkers();
  }


  loadMarkers() {
    var pubIcon = new leaflet.icon({
      iconUrl: "assets/imgs/markersIcons/pub_icon.png",
      iconSize:     [30, 40], // size of the icon
    });
    var restoIcon = new leaflet.icon({
      iconUrl: "assets/imgs/markersIcons/resto_icon.png",
      iconSize:     [30, 40], // size of the icon
    });
    var advantageIcon = new leaflet.icon({
      iconUrl: "assets/imgs/markersIcons/advantage_icon.png",
      iconSize:     [30, 40], // size of the icon
    });

    this.markersProvider.getBarMarkers().subscribe((bars: any) => {
      bars.forEach((singleBarMarker) => {
        var coord = [singleBarMarker.Coord.latitude, singleBarMarker.Coord.longitude];
        if (this.isLocationEnabled == true) {
          var dist = this.getDistance(this.userLocation, coord);
          var dformat = this.kmFormatter(dist);
          var photo
          firebase.storage().ref().child('Bar/'+singleBarMarker.Name +'Logo'+'.jpg').getDownloadURL().then((url) =>{
            photo = url;
            this.array.push({
              Name: singleBarMarker.Name,
              Type: 'BarPage',
              Distance: dist,
              City: singleBarMarker.City,
              Metric: dformat,
              Coord: coord,
              Photo: photo
            });
          })
        }
        else if (this.isLocationEnabled == false) {
          var photo
          firebase.storage().ref().child('Bar/'+singleBarMarker.Name +'Logo'+'.jpg').getDownloadURL().then((url) =>{
            photo = url;
            this.array.push({
              Name: singleBarMarker.Name,
              Type: 'BarPage',
              City: singleBarMarker.City,
              Coord: coord,
              Photo: photo
            });
          })
        }

        var popup = new leaflet.DomUtil.create('div', 'infoWindow');
        let marker: any = leaflet.marker([singleBarMarker.Coord.latitude, singleBarMarker.Coord.longitude], {icon: pubIcon}).addTo(this.map);
        this.translate.get(['MAP.MOREINFO']).subscribe(res => {
          popup.innerHTML = "<div class='divNamePopup'>" + singleBarMarker.Name + "</div><div class='divAddressPopup'>" + singleBarMarker.Address + ", " + singleBarMarker.City +
            "</div><div style='text-align: center;'><button id='info'>" + res["MAP.MOREINFO"] + "</button></div>";
        });
        marker.bindPopup(popup);

        $('#info', popup).on("click", () => {
          this.openBarPage(singleBarMarker);
        });
      });
    });

    this.markersProvider.getRestoMarkers().subscribe((resto: any) => {
      resto.forEach((singleRestoMarker) => {
        var coord = [singleRestoMarker.Coord.latitude, singleRestoMarker.Coord.longitude];
        if (this.isLocationEnabled == true) {
          var dist = this.getDistance(this.userLocation, coord);
          var dformat = this.kmFormatter(dist);
          var photo
          firebase.storage().ref().child('Resto/'+singleRestoMarker.Name +'Logo'+'.jpg').getDownloadURL().then((url) =>{
            photo = url;
            this.array.push({
              Name: singleRestoMarker.Name,
              Type: 'RestaurantPage',
              Distance: dist,
              City: singleRestoMarker.City,
              Metric: dformat,
              Coord: coord,
              Photo: photo
            });
          })
        }
        else if (this.isLocationEnabled == false) {
          var photo
          firebase.storage().ref().child('Resto/'+singleRestoMarker.Name +'Logo'+'.jpg').getDownloadURL().then((url) =>{
            photo = url;
            this.array.push({
              Name: singleRestoMarker.Name,
              Type: 'RestaurantPage',
              City: singleRestoMarker.City,
              Coord: coord,
              Photo: photo
            });
          })
        }

        var popup = new leaflet.DomUtil.create('div', 'infoWindow');
        let marker: any = leaflet.marker([singleRestoMarker.Coord.latitude, singleRestoMarker.Coord.longitude], {icon: restoIcon}).addTo(this.map);
        this.translate.get(['MAP.MOREINFO']).subscribe(res => {
          popup.innerHTML = "<div class='divNamePopup'>" + singleRestoMarker.Name + "</div><div class='divAddressPopup'>" + singleRestoMarker.Address + ", " + singleRestoMarker.City +
            "</div><div style='text-align: center;'><button id='info'>" + res["MAP.MOREINFO"] + "</button></div>";
        });
        marker.bindPopup(popup);

        $('#info', popup).on("click", () => {
          this.openRestoPage(singleRestoMarker);
        });
      });
    });

    this.markersProvider.getAdvantageMarkers().subscribe((advantage: any) => {
      advantage.forEach((singleAdvantageMarker) => {
        var coord = [singleAdvantageMarker.Coord.latitude, singleAdvantageMarker.Coord.longitude];
        if (this.isLocationEnabled == true) {
          var dist = this.getDistance(this.userLocation, coord);
          var dformat = this.kmFormatter(dist);
          var photo
          firebase.storage().ref().child('Avantage/'+singleAdvantageMarker.Name +'Logo'+'.jpg').getDownloadURL().then((url) =>{
            photo = url;
            this.array.push({
              Name: singleAdvantageMarker.Name,
              Type: 'AdvantagePage',
              Distance: dist,
              City: singleAdvantageMarker.City,
              Metric: dformat,
              Coord: coord,
              Photo: photo
            });
          })
        }
        else if (this.isLocationEnabled == false) {
          var photo
          firebase.storage().ref().child('Avantage/'+singleAdvantageMarker.Name +'Logo'+'.jpg').getDownloadURL().then((url) =>{
            photo = url;
            this.array.push({
              Name: singleAdvantageMarker.Name,
              Type: 'AdvantagePage',
              City: singleAdvantageMarker.City,
              Coord: coord,
              Photo: photo
            });
          })
        }

        var popup = new leaflet.DomUtil.create('div', 'infoWindow');
        let marker: any = leaflet.marker([singleAdvantageMarker.Coord.latitude, singleAdvantageMarker.Coord.longitude], {icon: advantageIcon}).addTo(this.map);
        this.translate.get(['MAP.MOREINFO']).subscribe(res => {
          popup.innerHTML = "<div class='divNamePopup'>" + singleAdvantageMarker.Name + "</div><div class='divAddressPopup'>" + singleAdvantageMarker.Address + ", " + singleAdvantageMarker.City +
            "</div><div style='text-align: center;'><button id='info'>" + res["MAP.MOREINFO"] + "</button></div>";
        });
        marker.bindPopup(popup);

        $('#info', popup).on("click", () => {
          this.openAdvantagePage(singleAdvantageMarker);
        });
      });
    });

    // this.array = sortingTable;
    console.log(this.array);
  }

  sortTable(){
    this.sortedArray = this.array.sort((a, b) => { return a["Distance"] - b["Distance"] });
    console.log(this.sortedArray);
  }


  getDistance(origin, destination) {
    // return distance in meters
    var lon1 = this.toRadian(origin[1]),
        lat1 = this.toRadian(origin[0]),
        lon2 = this.toRadian(destination[1]),
        lat2 = this.toRadian(destination[0]);

    var deltaLat = lat2 - lat1;
    var deltaLon = lon2 - lon1;

    var a = Math.pow(Math.sin(deltaLat/2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon/2), 2);
    var c = 2 * Math.asin(Math.sqrt(a));
    var EARTH_RADIUS = 6371;
    return Math.round(c * EARTH_RADIUS * 1000);
  }


  toRadian(degree) {
      return degree*Math.PI/180;
  }

  kmFormatter(num) {
    var format: any;
    return Math.abs(num) > 999 ? format = (Math.sign(num)*(Math.abs(num)/1000)).toFixed(1) + 'km' : format = Math.sign(num)*Math.abs(num) + 'm';
  }


  // networkMonitor(){
  //   this.translate.get(['NETWORK.CONNECTION', 'NETWORK.DISCONNECT'])
  //   .subscribe(res => {
  //     this.networkConnected = this.network.onDisconnect().subscribe(() => {
  //       //console.log('network was disconnected :-(');
  //       this.networkConnection = false;
  //       this.toast.create({
  //         message: res["NETWORK.DISCONNECT"],
  //         duration: 5000
  //       }).present();
  //     });
  //
  //     this.networkDisconnected = this.network.onConnect().subscribe(() => {
  //       setTimeout(() => {
  //         //console.log('we got a connection, woohoo!');
  //         this.networkConnection = true;
  //         this.toast.create({
  //           message: res["NETWORK.CONNECTION"] + this.network.type,
  //           duration: 5000
  //         }).present();
  //       }, 3000);
  //     });
  //   });
  // }



  openScanner(){
    this.translate.get([
      'HOMESCAN.ALERTCANCEL',
      'HOMESCAN.ALERT01CANCEL',
      'HOMESCAN.ALERT11SCANCANCEL',
      'HOMESCAN.ALERT12ERRORTITLE',
      'HOMESCAN.ALERT12ERRORSUB',
      'HOMESCAN.ALERT13VERIFTITLE',
      'HOMESCAN.ALERT13VERIFSUB',
      'HOMESCAN.ALERT13VERIFYENTERCODE',
      'HOMESCAN.ALERT14VERIFCANCEL',
      'HOMESCAN.ALERT15VERIFFALSETITLE',
      'HOMESCAN.ALERT15VERIFFALSESUB',
      'HOMESCAN.ALERT16VERIFSUCCESS',
      'HOMESCAN.ALERT17FREEUSED',
      'HOMESCAN.ALERT17FREESUB',
      'HOMESCAN.ALERT21ADVFALSETITLE',
      'HOMESCAN.ALERT21ADVFALSESUB',
      'HOMESCAN.ALERT21DRINKFALSETITLE',
      'HOMESCAN.ALERT21DRINKFALSESUB',
      'HOMESCAN.ALERT22OFFERSUSEDTITLE',
      'HOMESCAN.ALERT22OFFERSUSEDSUB'])
    .subscribe(res => {
      if(this.fyzz_free == true && this.isAdvSignUpActive == false){
        let alert = this.alertCtrl.create({
          title: res['HOMESCAN.ALERT17FREEUSED'],
          subTitle: res['HOMESCAN.ALERT17FREESUB'],
          buttons: [
            {
            text: res['HOMESCAN.ALERT01CANCEL'],
            role: 'cancel'
            },
            {
              text: "GO",
              handler: () => {
                this.navCtrl.push("AbonnementPage");
              }
            }
          ]
        });
        alert.present();
        return;
      }

      if(this.data == 'active' && (this.isDrinkActive == false && this.isAdvActive == false)){
        let alert = this.alertCtrl.create({
          title: res['HOMESCAN.ALERT22OFFERSUSEDTITLE'],
          subTitle: res['HOMESCAN.ALERT22OFFERSUSEDSUB'],
          buttons: ['OK']
        });
        alert.present();
        return;
      }

      this.options = {
        prompt: res["ABR.SCANQR"],
        resultDisplayDuration: 0,
        disableSuccessBeep: false
      }
      this.barcodeScanner.scan(this.options).then(barcodeData => {
        if(this.user){
          if(this.data == 'active' && (this.isDrinkActive == true || this.isAdvActive == true)){
            //Cancel the scan before doing anything
            if(barcodeData.cancelled == true){
              let alert = this.alertCtrl.create({
                title: res["HOMESCAN.ALERT11SCANCANCEL"],
                buttons: ['OK']
              });
              alert.present();
            }
            let qrCode = barcodeData.text.toString();
            //If QR code is in the collection ScanCodes on the DB
            var docRef = firebase.firestore().collection('ScanCodes').doc(qrCode);

            docRef.get().then((doc) => {
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    let codeVerif = doc.data().CodeVerif;
                    let name = doc.data().Name;
                    let type = doc.data().Type.AdvDrink; //Advantage or Drink?
                    let barResto = doc.data().Type.BarResto; //Bar or Resto?
                    let bcm = doc.data().BeerCoffeeMineral; //Beer, Coffee, Mineral?
                    let docID = doc.data().DocID;
                    //Shop Code Verification
                    let alert = this.alertCtrl.create({
                      title: res["HOMESCAN.ALERT13VERIFTITLE"],
                      enableBackdropDismiss: false,
                      subTitle: res["HOMESCAN.ALERT13VERIFSUB"],
                      inputs: [
                        {
                          name: "Code",
                          placeholder: res["HOMESCAN.ALERT13VERIFYENTERCODE"],
                          type: 'password',
                        }
                      ],
                      buttons: [
                        {
                          text: res["HOMESCAN.ALERTCANCEL"],
                          role: 'cancel',
                          handler: data =>{
                            alert.dismiss('annule');
                            return false;
                          }
                        },
                        {
                          text: 'OK',
                          handler: data =>{
                            //if CodeVerif is correct
                            if(data.Code == codeVerif){
                              alert.dismiss(true);
                              return false;
                            }else{
                              alert.dismiss(false);
                              return false;
                            }
                          }
                        }
                      ]
                    });

                    if(type == "Adv"){
                      if(this.isAdvActive == false){
                        let alert = this.alertCtrl.create({
                          title: res['HOMESCAN.ALERT21ADVFALSETITLE'],
                          subTitle: res['HOMESCAN.ALERT21ADVFALSESUB'],
                          buttons: ['OK']
                        });
                        alert.present();
                        return;
                      } else if(this.isAdvActive == true){
                        alert.present();
                      }
                    } else if(type == "Drink"){
                      if(this.isDrinkActive == false){
                        let alert = this.alertCtrl.create({
                          title: res['HOMESCAN.ALERT21DRINKFALSETITLE'],
                          subTitle: res['HOMESCAN.ALERT21DRINKFALSESUB'],
                          buttons: ['OK']
                        });
                        alert.present();
                        return;
                      } else if(this.isDrinkActive == true){
                        alert.present();
                      }
                    }



                    alert.onDidDismiss((data)=>{
                      //If Code Verif true
                      if(data==true){
                        let alert = this.alertCtrl.create({
                          title: res["HOMESCAN.ALERT16VERIFSUCCESS"],
                          buttons: ['OK']
                        });
                        alert.present();
                        //Increment the user counter
                        firebase.firestore().runTransaction(t => {
                          return t.get(this.userRef).then(doc => {
                            if (type == 'Adv'){
                              var newAdv = doc.data().counterAdv + 1;
                              t.update(this.userRef, { "counterAdv": newAdv, "scannerAdvBool": false });
                              document.getElementById('adv_banner_yes').style.background = 'linear-gradient(to left, #b3b3b3, #7d7777)';
                              this.isAdvActive = false;
                            } else if (type == 'Drink'){
                              var newDrink = doc.data().counterDrink + 1;
                              t.update(this.userRef, { "counterDrink": newDrink, "scannerDrinkBool": false});
                              document.getElementById('drink_banner_yes').style.background = 'linear-gradient(to left, #7d7777, #b3b3b3)';
                              this.isDrinkActive = false;
                            }
                          });
                        })
                        // increment the bar counter
                        if(type == 'Adv'){
                          var advRef = firebase.firestore().collection("Advantage").doc(docID);
                          firebase.firestore().runTransaction(t => {
                            return t.get(advRef).then(doc => {
                              console.log(advRef)
                              var newAdvScan = doc.data().CounterAdv + 1;
                              console.log("The COUNTERADV IS: " + newAdvScan);
                              t.update(advRef, { "CounterAdv": newAdvScan });
                            });
                          })
                        } else if (type == 'Drink'){
                          console.log("I AM A DRINKTYPE")
                          if (barResto == 'Bar'){
                            console.log("I AM A BAR")
                            var barRef = firebase.firestore().collection("Bar").doc(docID);
                            if(bcm == 'Beer'){
                              console.log("I AM A BEER")
                              firebase.firestore().runTransaction(t => {
                                return t.get(barRef).then(doc => {
                                  console.log(barRef);
                                  console.log("MY DOC ID is: " + docID);
                                  console.log(doc.data().City);
                                  //console.log("My beer counter is: " + doc.data().Counter.Beer);
                                  var newDrink = doc.data().Counter;
                                  var newDrinkB = newDrink.Beer + 1;
                                  t.update(barRef, { "Counter.Beer": newDrinkB });
                                });
                              })
                            } else if (bcm == 'Coffee'){
                              var transaction = firebase.firestore()
                              .runTransaction(t => {
                                return t.get(barRef).then(doc => {
                                  var newDrinkC = doc.data().Counter.Coffee + 1;
                                  t.update(barRef, { "Counter.Coffee": newDrinkC });
                                });
                              })
                            } else if (bcm == 'Mineral'){
                              var transaction = firebase.firestore()
                              .runTransaction(t => {
                                return t.get(barRef).then(doc => {
                                  var newDrinkM = doc.data().Counter.Mineral + 1;
                                  t.update(barRef, { "Counter.Mineral": newDrinkM });
                                });
                              })
                            }
                          } else if(barResto == "Resto"){
                            console.log("I AM A RESTO")
                            var barRef = firebase.firestore().collection("Resto").doc(docID);
                            if(bcm == 'Beer'){
                              console.log("I AM A BEER")
                              firebase.firestore().runTransaction(t => {
                                return t.get(barRef).then(doc => {
                                  console.log(barRef);
                                  console.log("MY DOC ID is: " + docID);
                                  console.log(doc.data().City);
                                  //console.log("My beer counter is: " + doc.data().Counter.Beer);
                                  var newDrinkB = doc.data().Counter.Beer + 1;
                                  t.update(barRef, { "Counter.Beer": newDrinkB });
                                });
                              })
                            } else if (bcm == 'Coffee'){
                              var transaction = firebase.firestore()
                              .runTransaction(t => {
                                return t.get(barRef).then(doc => {
                                  var newDrinkC = doc.data().Counter.Coffee + 1;
                                  t.update(barRef, { "Counter.Coffee": newDrinkC });
                                });
                              })
                            } else if (bcm == 'Mineral'){
                              var transaction = firebase.firestore()
                              .runTransaction(t => {
                                return t.get(barRef).then(doc => {
                                  var newDrinkM = doc.data().Counter.Mineral + 1;
                                  t.update(barRef, { "Counter.Mineral": newDrinkM });
                                });
                              })
                            }
                          }
                        }
                      }
                      //If code verif false or canceled
                      else {
                        //If code verif is false
                        if(data == false){
                          let alert = this.alertCtrl.create({
                            title: res["HOMESCAN.ALERT15VERIFFALSETITLE"],
                            subTitle: res["HOMESCAN.ALERT15VERIFFALSESUB"],
                            buttons: ['OK']
                          });
                          alert.present();
                        }
                        // else if(this.networkConnection == false){
                        //   let alert = this.alertCtrl.create({
                        //     title: "ERROR 404",
                        //     subTitle: res["NETWORK.TURNONCONNECTION"],
                        //     buttons: ['OK']
                        //   });
                        //   alert.present();
                        // }
                        else {
                          //If code verif is canceled
                          if('cancel'){
                            let alert = this.alertCtrl.create({
                              title: res["HOMESCAN.ALERT14VERIFCANCEL"],
                              buttons: ['OK']
                            });
                            alert.present();
                          }
                        }
                      }
                    })
                  }
                   else {
                    // The QR code does not exist in the data base!
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                    let alert = this.alertCtrl.create({
                      title: res["HOMESCAN.ALERT12ERRORTITLE"],
                      subTitle: res["HOMESCAN.ALERT12ERRORSUB"],
                      buttons: ['OK']
                    });
                    alert.present();
                  }
                  console.log('Barcode data', barcodeData);
            }).catch(function(error) {
                console.log("Error getting document:", error);
            });

          }else if (this.data == null && this.fyzz_free == true && this.isAdvSignUpActive == true){
            if(barcodeData.cancelled == true){
              let alert = this.alertCtrl.create({
                title: res["HOMESCAN.ALERT11SCANCANCEL"],
                buttons: ['OK']
              });
              alert.present();
            }
            let qrCode = barcodeData.text.toString();
            //If QR code is in the collection ScanCodes on the DB
            var docRef = firebase.firestore().collection('ScanCodes').doc(qrCode);

            docRef.get().then((doc) => {
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    let codeVerif = doc.data().CodeVerif;
                    let name = doc.data().Name;
                    let type = doc.data().Type.AdvDrink; //Advantage or Drink?
                    let barResto = doc.data().Type.BarResto; //Bar or Resto?
                    let bcm = doc.data().BCM; //Beer, Coffee, Mineral?
                    let docID = doc.data().DocID;

                    if (type == 'Adv'){
                      let alert = this.alertCtrl.create({
                        title: res["HOMESCAN.ALERT13VERIFTITLE"],
                        enableBackdropDismiss: false,
                        subTitle: res["HOMESCAN.ALERT13VERIFSUB"],
                        inputs: [
                          {
                            name: "Code",
                            placeholder: res["HOMESCAN.ALERT13VERIFYENTERCODE"],
                            type: 'password',
                          }
                        ],
                        buttons: [
                          {
                            text: res["HOMESCAN.ALERTCANCEL"],
                            role: 'cancel',
                            handler: data =>{
                              alert.dismiss('annule');
                              return false;
                            }
                          },
                          {
                            text: 'OK',
                            handler: data =>{
                              //if CodeVerif is correct
                              if(data.Code == codeVerif){
                                alert.dismiss(true);
                                return false;
                              }else{
                                alert.dismiss(false);
                                return false;
                              }
                            }
                          }
                        ]
                      });
                      alert.present();
                      alert.onDidDismiss((data)=>{
                        if(data==true){
                          let alert = this.alertCtrl.create({
                            title: res["HOMESCAN.ALERT16VERIFSUCCESS"],
                            buttons: ['OK']
                          });
                          alert.present();

                          // Increment the user's Adv counter
                          firebase.firestore().runTransaction(t => {
                            return t.get(this.userRef).then(doc => {
                              var newAdv = doc.data().counterAdv + 1;
                              t.update(this.userRef, { "counterAdv": newAdv, "scannerAdvBoolSignup": false });
                              document.getElementById('signup_banner').style.background = '#8b8c7f';
                              this.isAdvSignUpActive = false;
                            });
                          })

                          // increment the bar counter
                          var advRef = firebase.firestore().collection("Advantage").doc(docID);
                          firebase.firestore().runTransaction(t => {
                            return t.get(advRef).then(doc => {
                              var newAdvScan = doc.data().CounterAdv + 1;
                              t.update(advRef, { "CounterAdv": newAdvScan });
                            });
                          })
                        }
                        //IF code verif false or cancel
                        else {
                          if(data == false){
                            let alert = this.alertCtrl.create({
                              title: res["HOMESCAN.ALERT15VERIFFALSETITLE"],
                              subTitle: res["HOMESCAN.ALERT15VERIFFALSESUB"],
                              buttons: ['OK']
                            });
                            alert.present();
                          }
                          // else if(this.networkConnection == false){
                          //   let alert = this.alertCtrl.create({
                          //     title: "ERROR 404",
                          //     subTitle: res["NETWORK.TURNONCONNECTION"],
                          //     buttons: ['OK']
                          //   });
                          //   alert.present();
                          // }
                          else {
                            if('cancel'){
                              let alert = this.alertCtrl.create({
                                title: res["HOMESCAN.ALERT14VERIFCANCEL"],
                                buttons: ['OK']
                              });
                              alert.present();
                            }
                          }
                        }
                      })
                    } else {
                      let alert = this.alertCtrl.create({
                        title: res['HOMESCAN.ALERT17FREEUSED'],
                        subTitle: res['HOMESCAN.ALERT17FREESUB'],
                        buttons: [
                          {
                          text: res['HOMESCAN.ALERT01CANCEL'],
                          role: 'cancel'
                          },
                          {
                            text: "GO",
                            handler: () => {
                              this.navCtrl.push("AbonnementPage");
                            }
                          }
                        ]
                      });
                      alert.present();
                    }

                  }
                   else {
                    // The QR code does not exist in the data base!
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                    let alert = this.alertCtrl.create({
                      title: res["HOMESCAN.ALERT12ERRORTITLE"],
                      subTitle: res["HOMESCAN.ALERT12ERRORSUB"],
                      buttons: ['OK']
                    });
                    alert.present();
                  }
                  console.log('Barcode data', barcodeData);
            }).catch(function(error) {
                console.log("Error getting document:", error);
            });
          }
          // else if (this.networkConnection == false){
          //   let alert = this.alertCtrl.create({
          //     title: "ERROR 404",
          //     subTitle: res["NETWORK.TURNONCONNECTION"],
          //     buttons: ['OK']
          //   });
          //   alert.present();
          // }
          // else{
          //   let alert = this.alertCtrl.create({
          //     title: res["ABR.SCANACTIVATION"],
          //     subTitle: res["ABR.SCANACTIVATIONTXT"],
          //     buttons: ['OK']
          //   });
          //   alert.present();
          // }
        }
        else{
          let alert = this.alertCtrl.create({
            title: "ERROR404",
            subTitle: "Scan not found!",
            buttons: ['OK']
          });
          alert.present();
        }
      }).catch(err => {
        console.log('Error', err);
      });
    });
  }



  openSignUpAlert(){
    this.translate.get(['HOMESCAN.ALERT01TITLE','HOMESCAN.ALERT01SUB','HOMESCAN.ALERT01CANCEL'])
    .subscribe(res => {
      let alert = this.alertCtrl.create({
        title: res['HOMESCAN.ALERT01TITLE'],
        subTitle: res['HOMESCAN.ALERT01SUB'],
        buttons: [
          {
            text: res['HOMESCAN.ALERT01CANCEL'],
            role: 'cancel'
          },
          {
            text: "GO",
            handler: () => {
              this.navCtrl.push("SignupPage");
            }
          }
        ]
      });
      alert.present();
    });
  }

}
