import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertController, NavController, IonicPage, LoadingController} from 'ionic-angular';
import * as firebase from 'firebase';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'reset-password',
  templateUrl: 'reset-password.html',
})

@Injectable()
export class ResetPasswordPage {
  public resetPasswordForm: FormGroup;
  email: string;

  constructor(
      public navCtrl: NavController,
      public alertCtrl: AlertController,
      public formBuilder: FormBuilder,
      public loadingCtrl: LoadingController,
      public translate: TranslateService,
  ) {
    this.resetPasswordForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')])],
    });
  }


  resetPassword(): void {
    this.translate.get(['RESETPWD.ALERTTITLE','RESETPWD.ALERTTXT1','RESETPWD.ALERTTXT2'])
    .subscribe(res => {
      if (!this.resetPasswordForm.valid) {
        console.log(`Form is not valid yet, current value: ${
          this.resetPasswordForm.value}`);
      } else {
        this.email = this.resetPasswordForm.value.email;

        var auth = firebase.auth();

        auth.sendPasswordResetEmail(this.email)
        .then(function() {
          // Email sent.
          console.log('Password reset email sent.');
        })
        .catch(function(error) {
          console.log(
            'An error occured while sending the password reset request.');
          });
        }
        let alert = this.alertCtrl.create({
          title: res["RESETPWD.TITLE"],
          subTitle: res["RESETPWD.ALERTTXT1"] +' ' + this.email +'.' + res["RESETPWD.ALERTTXT2"],
          buttons: ['OK']
        });
        alert.present();
    })
  }
}
