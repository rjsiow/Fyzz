import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';

/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {
  userLang: string;
  dateLang: string;
  array:Array<any> = [];
  sortedArray: Array<any> = [];
  photo: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public nativeStorage: NativeStorage,
    private afs: AngularFirestore) {

  }


  getLanguage() {
    this.nativeStorage.getItem('userSettings')
      .then(
        data => {
          this.userLang = data.language;
          // if(this.userLang == 'de') {
          //   this.dateLang = 'de-DE';
          // } else if(this.dateLang == 'en') {
          //   this.dateLang = 'en-US';
          // } else if(this.dateLang == 'fr') {
          //   this.dateLang = 'fr-FR';
          // } else if( this.dateLang == 'it') {
          //   this.dateLang = 'it-IT';
          // }
        },
        error => console.error(error)
      );
  }


  ionViewCanEnter() {
    //this.loadData();
    this.getLanguage();
    console.log("The phone language is: " + this.userLang)
    console.log("The date language is: " + this.dateLang)
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad EventsPage');
    //this.sortTableTime();
    this.loadData();
  }

  openSettings(){
      this.navCtrl.push('SettingsPage');
  }

  openBarPage(barInfo){
    this.navCtrl.push('BarPage', barInfo);
  }


  openRestoPage(restoInfo){
    this.navCtrl.push('RestaurantPage', restoInfo);
  }


  openAdvantagePage(advantageInfo){
    this.navCtrl.push('AdvantagePage', advantageInfo);
  }


  loadData() {
    console.log("22222The date language is: " + this.dateLang);
    this.afs.collection('Events').valueChanges().subscribe((events: any) => {
      events.forEach((establishment) => {
        var photo;
        var type = establishment.Type;
        var name = establishment.Name;
        var startDate = establishment.StartDate.toDate();
        var endDate = establishment.EndDate.toDate();
        var startDateBlur = this.dateCompare(startDate);
        var validEndDate = this.dateCompare(endDate);
        var options = {year: 'numeric', month: 'numeric', day: 'numeric'}

        if(validEndDate == true) {
          firebase.storage().ref().child(type+'/'+name+'Logo.jpg').getDownloadURL().then((url) => {
            photo = url;
            this.array.push({
              IsBlur: startDateBlur,
              Name: name,
              Type: type,
              Description_de: establishment.Description_de,
              Description_en: establishment.Description_en,
              Description_fr: establishment.Description_fr,
              Description_it: establishment.Description_it,
              StartDate: startDate,
              EndDate: endDate,
              FormatedStartDate: startDate.toLocaleDateString(options),
              FormatedEndDate: endDate.toLocaleDateString(options),
              Photo: photo
            });
            this.sortTableTime();
          });
        } else {
          // this.afs.collection('Events').doc(establishment.id).delete().then(function() {
          //   console.log("Document successfully deleted!");
          // }).catch(function(error) {
          //   console.error("Error removing document: ", error);
          // });
          console.log("The event is outdated!");
        }
      });
    });
    console.log("here is the array");
    console.log(this.array);
  }

  sortTableTime() {
    this.sortedArray = this.array.sort((a, b) => { return a["StartDate"].getTime() - b["StartDate"].getTime()});
    console.log("This is the sorted Array");
    console.log(this.sortedArray);
  }

  dateCompare(date){
    var today = new Date();
    if(date > today){
      return true;
    } else {
      return false;
    }
  }


  openPage(id){
    console.log("The type is: " + id.Type)
    if (id.Type == 'Bar') {
      this.openBarPage(id)
    }
    else if (id.Type == 'Resto') {
      this.openRestoPage(id)
    }
    else if (id.Type == 'Avantage') {
      this.openAdvantagePage(id)
    }
    // console.log("item clicked is:%s"+ id);
  }
}
