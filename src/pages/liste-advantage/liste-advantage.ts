import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';


@IonicPage()
@Component({
  selector: 'page-liste-advantage',
  templateUrl: 'liste-advantage.html',
})
export class ListeAdvantagePage {

  advcollection: AngularFirestoreCollection<any> = this.afs.collection('Advantage');
  advobs: any;
  arrayQuery: Array<any> = [];
  array2: Array<any> = [];
  searchTerm: string = '';
  searching: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private afs: AngularFirestore) {

      let array = [];
      this.advcollection.ref.orderBy("Name", "asc").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          array.push({
            Name: doc.data().Name,
            Address: doc.data().Address,
            City: doc.data().City
          });
        });
      });
      this.arrayQuery = array;
      console.log(array);
      console.log(this.arrayQuery);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ListeAdvantagePage');
    this.setFilteredItems();
    console.log(this.searching);
  }


  openSettings(){
    this.navCtrl.push('SettingsPage');
  }


  openAdvantage(advantageInfo){
    this.navCtrl.push('AdvantagePage', advantageInfo);
  }


  filterItems(searchTerm){
    return this.arrayQuery.filter((item) => {
        return item.Name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }


  setFilteredItems() {
    this.array2 = this.filterItems(this.searchTerm);
    if(this.array2[0] != null){
      this.searching = true;
    }
    console.log(this.array2);
  }
}
