import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, ToastController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';
import { Subscription} from 'rxjs/Subscription';


@IonicPage()
@Component({
  selector: 'page-bar',
  templateUrl: 'bar.html',
})

export class BarPage {
  userLang: string;

  networkConnection: boolean;
  networkConnected: Subscription;
  networkDisconnected: Subscription;

  barAddress: string;
  barCity: string;
  barName: string;
  barInfo: any;
  barBeer: string;
  barCoffee: string;
  barMineral: string;
  barCode: string;
  description: string;
  photoName1: string;
  photoName2: string;
  photoName3: string;
  photo1;
  photo2;
  photo3;
  barId: string;
  show_hours = false;

  barInfoCollection: any;
  barInfoObs: any;

  user: any;
  userRef: any;
  disableButton: boolean;
  scannerBool: boolean = false;
  scanText: string = '';

  public login: boolean = false;
  payment_status: string;

  options: BarcodeScannerOptions;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public translate: TranslateService,
    public navParams: NavParams,
    private afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    private barcodeScanner: BarcodeScanner,
    public nativeStorage: NativeStorage,
    public network: Network) {

  }


  getLanguage() {
    this.nativeStorage.getItem('userSettings')
      .then(
        data => {
          this.userLang = data.language;
        },
        error => console.error(error)
      );
  }


  ionViewCanEnter(){
    this.getLanguage();
    console.log("The network in ionViewCanEnter is " + this.networkConnection);

    this.barInfo = this.navParams.data;
    this.barInfoCollection = this.afs.collection<any>('Bar', ref =>
      ref.where('Name', '==', this.barInfo.Name));
    this.barInfoObs = this.barInfoCollection.valueChanges();

    this.loadData();
    this.photoName1 = this.barInfo.Name +'1';
    this.photoName2 = this.barInfo.Name +'2';
    this.photoName3 = this.barInfo.Name +'3';
    this.getPhotoURL();

    this.barInfoCollection.ref.where('Name', '==', this.barInfo.Name).get().then(async querySnapshot =>{
      await querySnapshot.forEach(async doc => {
        this.barBeer = await doc.data().CodeB;
        this.barCoffee = await doc.data().CodeC;
        this.barMineral = await doc.data().CodeM;
        this.barCode = await doc.data().CodeVerif;
        this.barId = await doc.id;
      });
    });
  }


  // ionViewWillEnter(){
  //   this.networkMonitor();
  // }


  ionViewDidEnter(){
    this.networkMonitor();
  }


  ionViewWillLeave(){
    this.networkConnection = false;
    //console.log("The connection is " + this.networkConnection);
    this.networkConnected.unsubscribe();
    this.networkDisconnected.unsubscribe();
  }

  // ionViewDidLeave(){
  //   this.networkConnection = false;
  //   console.log("The connection is " + this.networkConnection);
  // }


  networkMonitor(){
    this.translate.get(['NETWORK.CONNECTION', 'NETWORK.DISCONNECT'])
    .subscribe(res => {
      this.networkConnected = this.network.onDisconnect().subscribe(() => {
        //console.log('network was disconnected :-(');
        this.networkConnection = false;
        this.toast.create({
          message: res["NETWORK.DISCONNECT"],
          duration: 5000
        }).present();
      });

      this.networkDisconnected = this.network.onConnect().subscribe(() => {
        setTimeout(() => {
          //console.log('we got a connection, woohoo!');
          this.networkConnection = true;
          this.toast.create({
            message: res["NETWORK.CONNECTION"] + this.network.type,
            duration: 5000
          }).present();
        }, 3000);
      });
    });
  }


  loadData(){
    this.afAuth.authState.subscribe(async userf => {
      this.user = userf;
      if(this.user)  {
        this.login = true;
        var paymentRef = await firebase.firestore().collection('User').doc(this.user.uid).collection('Stripe').doc('Abonnement');
        paymentRef.get().then(async doc => {
          if(doc.exists){
            this.payment_status = await doc.data().fyzz_standard;
            console.log("PAYMENT STATUS IS: " + this.payment_status);
            this.networkConnection = true;
            //console.log("The network in loadData is " + this.networkConnection);

            if(this.payment_status != 'active'){
              this.disableButton = false;
            }
          } else {
            console.log('No such document!');
          }
        });

        this.userRef = await firebase.firestore().collection('User').doc(this.user.uid);
        this.userRef.get().then(async doc => {
          this.scannerBool = await doc.data().scannerDrinkBool;
          if(this.scannerBool == true){
            this.disableButton = false;
            this.scanText = "scan_enable";
          }
          else if(this.scannerBool == false){
            this.disableButton = true;
            this.scanText = "scan_disable";
          }
        });
      } else {
        this.login = false;
      }
    });
  }


  openSignUp(){
    this.navCtrl.push('SignupPage');
  }


  openAboPage(){
      this.navCtrl.push('AbonnementPage');
  }


  openSettings(){
      this.navCtrl.push('SettingsPage');
  }


  openHome(){
    this.navCtrl.push('HomePage');
  }


  openContact(){
    this.navCtrl.push('ContactPage');
  }

  toggleHours() {
    if (this.show_hours == false) {
      this.show_hours = true;
    }
    else {
      this.show_hours = false;
    }
  }


  openScanner(){
    this.translate.get(['ABR.SCANQR','ABR.CANCEL','ABR.VERIFY','ABR.VERIFYTXTDRINK','ABR.VERIFYCODE','ABR.VERIFYENTERCODE','ABR.VERIFYCANCEL','ABR.VERIFYCANCELTXT','ABR.DRINKVALIDATED','ABR.DRINKVALIDATEDTXT','ABR.SCANCANCEL','ABR.SCANERROR','ABR.SCANERRORTXT','ABR.SCANACTIVATION','ABR.SCANACTIVATIONTXT','ABR.SCANSUBSCRIBE','ABR.SCANSUBSCRIBETXT', 'NETWORK.TURNONCONNECTION'])
    .subscribe(res => {
      this.options = {
        prompt: res["ABR.SCANQR"],
        resultDisplayDuration: 0,
        disableSuccessBeep: false
      }
      this.barcodeScanner.scan(this.options).then(barcodeData => {
        if(this.user){
          if(this.payment_status == 'active' && this.networkConnection == true){
            let qrCode = barcodeData.text.toString();
            if(this.barBeer == qrCode || this.barCoffee == qrCode || this.barMineral == qrCode){
              let alert = this.alertCtrl.create({
                title: res["ABR.VERIFY"],
                enableBackdropDismiss: false,
                subTitle: res["ABR.VERIFYTXTDRINK"],
                inputs: [
                  {
                    name: "Code",
                    placeholder: res["ABR.VERIFYENTERCODE"],
                    type: 'password',
                  }
                ],
                buttons: [
                  {
                    text: res["ABR.CANCEL"],
                    role: 'cancel',
                    handler: data =>{
                      alert.dismiss('annule');
                      return false;
                    }
                  },
                  {
                    text: 'OK',
                    handler: data =>{
                      if(data.Code == this.barCode){
                        alert.dismiss(true);
                        return false;
                      }else{
                        alert.dismiss(false);
                        return false;
                      }
                    }
                  }
                ]
              });
              alert.present();
              alert.onDidDismiss((data)=>{
                if(data==true && this.networkConnection == true){
                  let alert = this.alertCtrl.create({
                    title: res["ABR.DRINKVALIDATED"],
                    subTitle: res["ABR.DRINKVALIDATEDTXT"],
                    buttons: ['OK']
                  });
                  alert.present();
                  //Increment the user counter
                  var transaction = firebase.firestore()
                  .runTransaction(t => {
                    return t.get(this.userRef).then(doc => {
                      var newDrink = doc.data().counterDrink + 1;
                      t.update(this.userRef, { "counterDrink": newDrink, "scannerDrinkBool": false});
                      this.disableButton = true;
                      console.log("SECOND LOG IS: " + this.description +  " " + this.userLang);
                    });
                  })
                  console.log("FIRST LOG IS: " + this.description +  " " + this.userLang);
                  // increment the bar counter
                  if(this.barBeer == qrCode){
                    var barRef = firebase.firestore().collection("Bar").doc(this.barId);
                    var transaction = firebase.firestore()
                    .runTransaction(t => {
                      return t.get(barRef).then(doc => {
                        var newDrinkB = doc.data().Counter.Beer + 1;
                        t.update(barRef, { "Counter.Beer": newDrinkB });
                      });
                    })
                  }
                  if(this.barCoffee == qrCode){
                    var barRef = firebase.firestore().collection("Bar").doc(this.barId);
                    var transaction = firebase.firestore()
                    .runTransaction(t => {
                      return t.get(barRef).then(doc => {
                        var newDrinkC = doc.data().Counter.Coffee + 1;
                        t.update(barRef, { "Counter.Coffee": newDrinkC });
                      });
                    })
                  }
                  if(this.barMineral == qrCode){
                    var barRef = firebase.firestore().collection("Bar").doc(this.barId);
                    var transaction = firebase.firestore()
                    .runTransaction(t => {
                      return t.get(barRef).then(doc => {
                        var newDrinkM = doc.data().Counter.Mineral + 1;
                        t.update(barRef, { "Counter.Mineral": newDrinkM });
                      });
                    })
                  }
                } else {
                  if(data == false){
                    let alert = this.alertCtrl.create({
                      title: res["ABR.VERIFYCANCEL"],
                      subTitle: res["ABR.VERIFYCANCELTXT"],
                      buttons: ['OK']
                    });
                    alert.present();
                  } else if(this.networkConnection == false){
                    let alert = this.alertCtrl.create({
                      title: "ERROR 404",
                      subTitle: res["NETWORK.TURNONCONNECTION"],
                      buttons: ['OK']
                    });
                    alert.present();
                  }
                  else {
                    if('cancel'){
                      let alert = this.alertCtrl.create({
                        title: res["ABR.VERIFYCANCEL"],
                        buttons: ['OK']
                      });
                      alert.present();
                    }
                  }
                }
              })
            } else {
              if(barcodeData.cancelled == true){
                let alert = this.alertCtrl.create({
                  title: res["ABR.SCANCANCEL"],
                  buttons: ['OK']
                });
                alert.present();
              } else {
                let alert = this.alertCtrl.create({
                  title: res["ABR.SCANERROR"],
                  subTitle: res["ABR.SCANERRORTXT"],
                  buttons: ['OK']
                });
                alert.present();
              }
            }
            console.log('Barcode data', barcodeData);
          }
          else if (this.networkConnection == false){
            let alert = this.alertCtrl.create({
              title: "ERROR 404",
              subTitle: res["NETWORK.TURNONCONNECTION"],
              buttons: ['OK']
            });
            alert.present();
          }
          else{
            let alert = this.alertCtrl.create({
              title: res["ABR.SCANACTIVATION"],
              subTitle: res["ABR.SCANACTIVATIONTXT"],
              buttons: ['OK']
            });
            alert.present();
          }
        }
        else{
          let alert = this.alertCtrl.create({
            title: res["ABR.SCANSUBSCRIBE"],
            subTitle: res["ABR.SCANSUBSCRIBETXT"],
            buttons: ['OK']
          });
          alert.present();
        }
      }).catch(err => {
        console.log('Error', err);
      });
    });
    console.log("LAST LOG IS: " + this.description +  " " + this.userLang);
  }


  getPhotoURL(){
    firebase.storage().ref().child('Bar/'+this.photoName1+'.jpg').getDownloadURL()
    .then((url) =>{
      this.photo1 = url;
    })
    firebase.storage().ref().child('Bar/'+this.photoName2+'.jpg').getDownloadURL()
    .then((url) =>{
      this.photo2 = url;
    })
    firebase.storage().ref().child('Bar/'+this.photoName3+'.jpg').getDownloadURL()
    .then((url) =>{
      this.photo3 = url;
    })
  }
}
