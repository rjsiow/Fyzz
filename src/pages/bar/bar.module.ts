import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BarPage } from './bar';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

@NgModule({
  declarations: [
    BarPage,
  ],
  imports: [
    IonicPageModule.forChild(BarPage),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    BarPage
  ]
})
export class BarPageModule {}
