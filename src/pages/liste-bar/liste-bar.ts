import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import firebase from 'firebase';


@IonicPage()
@Component({
  selector: 'page-liste-bar',
  templateUrl: 'liste-bar.html',
})
export class ListeBarPage {

  barcollection: AngularFirestoreCollection<any> = this.afs.collection('Bar');
  barobs: any;
  arrayQuery: Array<any> = [];
  array2: Array<any> = [];
  searchTerm: string = '';
  searching: boolean = false;
  photo1;

  constructor(
    private afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams){
      let array = [];
      this.barcollection.ref.orderBy("Name", "asc").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          array.push({
            Name: doc.data().Name,
            Address: doc.data().Address,
            City: doc.data().City
          });
        });
      });
      this.arrayQuery = array;
      console.log(array);
      console.log(this.arrayQuery);
    }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ListeBarPage');
    this.setFilteredItems();
    console.log(this.searching);
  }


  openSettings(){
    this.navCtrl.push('SettingsPage');
  }


  openBar(barInfo){
    this.navCtrl.push('BarPage', barInfo);
  }


  filterItems(searchTerm){
    return this.arrayQuery.filter((item) => {
        return item.Name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }


  setFilteredItems() {
    this.array2 = this.filterItems(this.searchTerm);
    if(this.array2[0] != null){
      this.searching = true;
    }
    console.log(this.array2);
  }

  getPhotoURL(barName: string){
    firebase.storage().ref().child('Bar/'+ barName +'1.jpg').getDownloadURL()
    .then((url) =>{
      this.photo1 = url;
    })
  }
}
