import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';


@IonicPage()
@Component({
  selector: 'page-liste-restaurant',
  templateUrl: 'liste-restaurant.html',
})
export class ListeRestaurantPage {

  restaurantcollection: AngularFirestoreCollection<any> =this.afs.collection('Resto');
  restaurantobs = this.restaurantcollection.valueChanges();
  arrayQuery: Array<any> = [];
  array2: Array<any> = [];
  searchTerm: string = '';
  searching: boolean = false;

  constructor(
    private afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams) {
      let array = [];
      this.restaurantcollection.ref.orderBy("Name", "asc").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          array.push({
            Name: doc.data().Name,
            Address: doc.data().Address,
            City: doc.data().City
          });
        });
      });
      this.arrayQuery = array;
      console.log(array);
      console.log(this.arrayQuery);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ListeRestaurantPage');
  }


  openSettings(){
    this.navCtrl.push('SettingsPage');
  }


  openRestaurant(restoInfo){
    this.navCtrl.push('RestaurantPage', restoInfo);
  }


  filterItems(searchTerm){
    return this.arrayQuery.filter((item) => {
        return item.Name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

  setFilteredItems() {
    this.array2 = this.filterItems(this.searchTerm);
    if(this.array2[0] != null){
      this.searching = true;
    }
    console.log(this.array2);
  }
}
