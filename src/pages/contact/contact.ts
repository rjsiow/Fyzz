import 'firebase/firestore';
import { Component } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { TranslateService } from '@ngx-translate/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import firebase from 'firebase/app';
import { ActionSheetController, IonicPage, NavController, AlertController, LoadingController, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { environment } from '../../app/credentials';
import { PaymentService } from '../../services/payment.service';


@IonicPage()
@Component({selector: 'page-contact', templateUrl: 'contact.html'})
export class ContactPage {

  userInfo: any;
  userInfoCollection: any;
  userInfoObs: any;
  statusInfoCollection: any;
  statusObs: any;
  myPhoto: string;
  handler: any;  // stripe handler
  handlerUpdate: any;  // stripe handler for update card
  status:string = '';
  loading: any;
  userEmail: string;
  freeAbo_status: any;
  payment_status: string = '';
  userData: Array<any> = [];
  //count: number;
  isUserVerified: boolean;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public platform: Platform,
      public nativeStorage: NativeStorage,
      public translate: TranslateService,
      public afAuth: AngularFireAuth,
      public alertCtrl: AlertController,
      public loadingCtrl: LoadingController,
      private afs: AngularFirestore,
      private pmt: PaymentService,
      public actionSheetController: ActionSheetController) {

      /*let count = this.afs.collection(`/User`)
                 .snapshotChanges()
                 .subscribe(c => {
                   this.count = c.length;
                 });*/
    }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
    this.loadData();
  }

  openAboPage(){
      this.navCtrl.push('AbonnementPage');
  }


  ionViewCanEnter(){
    //this.configHandler();
    this.configHandlerUpdate();
  }


  loadData(){
    this.afAuth.authState.subscribe(async user => {
      if (user) {
        this.isUserVerified = await user.emailVerified;
        if(this.isUserVerified == false){
          await user.reload();
          await user.getIdToken(true);
          this.isUserVerified = await user.emailVerified;
        }

        console.log("Is the user verified? " + this.isUserVerified);
        var docRef = await firebase.firestore().collection('User').doc(user.uid);
        docRef.get().then(async doc => {
          if (doc.exists) {
            var data = await doc.data();
            console.log("The User Data is: ", this.userData);
            this.userData.push({
              firstname: data.firstname,
              lastname: data.lastname,
              birthdate: data.birthdate,
              email: data.email,
              npa: data.postalCode,
              city: data.city
            });
            this.freeAbo_status = doc.data().fyzz_free;
            this.userEmail = doc.data().email;
          } else {
            console.log('No such document!');
          }
        })
        .catch(function(error) {
          console.log('Error getting document:', error);
        });

        var statusRef = await firebase.firestore().collection('User').doc(user.uid)
        .collection('Stripe').doc('Abonnement')
        .get().then(async doc => {
          if (doc.exists) {
            this.payment_status = await doc.data().fyzz_standard;
          } else {
            console.log('No such document!');
          }
        })
        .catch(function(error) {
          console.log('Error getting document:', error);
        });
      }
    });
  }


  /*showLoadingAlert(){
    this.translate.get(['CONTACT.SUBSCRIPTIONACTIVATION','CONTACT.SUCCESS','CONTACT.SUCCESSTXT','CONTACT.ERROR','CONTACT.ERRORTXT'])
    .subscribe(res => {
      this.loading = this.loadingCtrl.create({
        content: res["CONTACT.SUBSCRIPTIONACTIVATION"],
        duration: 13000
      });

      this.loading.present();
      this.loading.onDidDismiss(()=>{
        this.afAuth.authState.subscribe(user => {
          if (user) {
            var statusDoc = firebase.firestore().collection('User').doc(user.uid)
            .collection('Stripe').doc('Abonnement')
            .get().then( doc => {
              if (doc.exists) {
                let alert = this.alertCtrl.create({
                  title: res["CONTACT.SUCCESS"],
                  subTitle: res["CONTACT.SUCCESSTXT"],
                  buttons: [
                    {
                      text: 'OK',
                      handler: () =>{
                        this.navCtrl.setRoot('MapPage');
                        this.showTutorialAlert();
                      }
                    }
                  ]
                });
                alert.present();
              } else {
                let alert = this.alertCtrl.create({
                  title: res["CONTACT.ERROR"],
                  subTitle: res["CONTACT.ERRORTXT"],
                  buttons: [
                    {
                      text: 'OK',
                    }
                  ]
                });
                alert.present();
              }
            })
          }
        })
      })
    });
  }*/

  /*showTutorialAlert(){
    this.translate.get(['CONTACT.TUTORIAL', 'CONTACT.STEPS'])
    .subscribe(res => {
      let alert = this.alertCtrl.create({
        title: res["CONTACT.TUTORIAL"],
        enableBackdropDismiss: false,
        message: "<div class='textNotif'>" + res["CONTACT.STEPS"] + "</div>",
        buttons: [
          {
            text: 'OK',
            handler: () =>{
            }
          }
        ]
      });
      alert.present();
    });
  }*/

  showLoadingAlertUpdate(){
    this.translate.get(['CONTACT.LOADINGUPDATECARD','CONTACT.UPDATETITLEOK','CONTACT.UPDATETEXTOK','CONTACT.UPDATETITLEERROR','CONTACT.UPDATETEXTERROR'])
    .subscribe(res => {
      this.loading = this.loadingCtrl.create({
        content: res["CONTACT.LOADINGUPDATECARD"],
        duration: 13000
      });
      this.loading.present();
      this.loading.onDidDismiss(()=>{
        this.afAuth.authState.subscribe(async user => {
          if (user) {
            var statusDoc = await firebase.firestore().collection('User').doc(user.uid)
            .collection('Source').doc('SourceStatus')
            .get().then(async doc => {
                var data = await doc.data();
                var statusUpdate = await data.sourceStatus;
                if(statusUpdate == 'ok'){
                let alert = this.alertCtrl.create({
                  title: res["CONTACT.UPDATETITLEOK"],
                  subTitle: res["CONTACT.UPDATETEXTOK"],
                  enableBackdropDismiss: false,
                  buttons: [
                    {
                      text: 'OK',
                      handler: () =>{
                        this.navCtrl.setRoot('MapPage');
                      }
                    }
                  ]
                });
                alert.present();
              } else {
                let alert = this.alertCtrl.create({
                  title: res["CONTACT.UPDATETITLEERROR"],
                  subTitle: res["CONTACT.UPDATETEXTERROR"] + '<a href="mailto:info@fyzz.ch">info@fyzz.ch</a>',
                  buttons: [
                    {
                      text: 'OK',
                    }
                  ]
                });
                alert.present();
              }
            })
          }
        })
      })
    });
  }


  /*async stripeSubscribe() {
    this.translate.get(['CONTACT.CURRENCY','CONTACT.EMAILVALIDATE','CONTACT.EMAILVALIDATETXT'])
    .subscribe(res => {
      if(this.isUserVerified == true){
        if(this.count <= 100){
          this.handler.open({
            name: 'FYZZ',
            description: "9,90 " + res["CONTACT.CURRENCY"],
            email: this.userEmail,
            amount: 990,
            currency: 'CHF'
          });
        }else{
          if(100 < this.count && this.count <= 500){
            this.handler.open({
              name: 'FYZZ',
              description: "14,90 " + res["CONTACT.CURRENCY"],
              email: this.userEmail,
              amount: 1490,
              currency: 'CHF'
            });
          }else{
            this.handler.open({
              name: 'FYZZ',
              description: "19,90 " + res["CONTACT.CURRENCY"],
              email: this.userEmail,
              amount: 1990,
              currency: 'CHF'
            });
          }
        }
        this.platform.backButton.subscribe(() => {
          // close alert
          if(this.handler){
            try {
              this.handler.close();
            } catch (error) {
            }
          }
        });
      }else{
        let alert = this.alertCtrl.create({
          title: res["CONTACT.EMAILVALIDATE"],
          subTitle: res["CONTACT.EMAILVALIDATETXT"],
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                console.log('Confirm Okay');
              }
            }
          ]
        });
        alert.present();
      }
    });
  }*/

  /*private configHandler() {
    this.handler = StripeCheckout.configure({
      key: environment.stripeKey,
      image: 'assets/imgs/logo_checkout.png',
      locale: 'auto',
      allowRememberMe: false,
      token: token => {
        this.pmt.processPayment(token);
        this.showLoadingAlert();
      }
    });
  }*/

  //------------------------------------------------------------------------
  // ------------------- CARD UPDATE----------------------------

  async UpdateCard() {
    this.translate.get(['CONTACT.DESCRIPTIONUPDATE','CONTACT.BUTTONUPDATE'])
    .subscribe(res => {
        this.handlerUpdate.open({
          name: 'FYZZ',
          description: res["CONTACT.DESCRIPTIONUPDATE"],
          email: this.userEmail,
          panelLabel: res["CONTACT.BUTTONUPDATE"]
        });
        this.platform.backButton.subscribe(() => {
          // close alert
          if(this.handlerUpdate){
            try {
              this.handlerUpdate.close();
            } catch (error) {
            }
          }
        });
      })
    }

  private configHandlerUpdate() {
    this.handlerUpdate = StripeCheckout.configure({
      key: environment.stripeKey,
      image: 'assets/imgs/logo_checkout.png',
      locale: 'auto',
      allowRememberMe: false,
      token: token => {
        this.pmt.tokenUpdateCard(token);
        this.showLoadingAlertUpdate();
        //mettre une alerte pour dire si c'est okay ou pas
      }
    });
  }

//----------------------------------------------------------------------------------

  logout() {
    this.translate.get(['CONTACT.LOGOUT', 'CONTACT.LGCONFIRM', 'CONTACT.CANCEL']).subscribe(res => {
      let alert = this.alertCtrl.create({
        title: res["CONTACT.LOGOUT"],
        subTitle: res["CONTACT.LGCONFIRM"],
        buttons: [
          {
            text: res["CONTACT.CANCEL"],
            role: 'cancel',
            handler: () =>{
            }
          },
          {
            text: 'Ok',
            handler: () => {
              console.log('Confirm Okay');
              this.afAuth.auth.signOut();
              this.navCtrl.setRoot('MapPage');
            }
          }
        ]
      });
      alert.present();
    });
  }
}
