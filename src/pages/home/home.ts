import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, Platform } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public isLocationEnabled: boolean = null;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController) {
  }


  openLogin() {
    this.navCtrl.push('ConnexionPage');
  }
  // openMapPage() {
  //   this.navCtrl.push('MapPage');
  // }

  openSettings() {
    this.navCtrl.push('SettingsPage');
  }

}
