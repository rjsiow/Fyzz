import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { MapPage } from '../map/map';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import firebase from 'firebase/app';
import 'firebase/firestore';

/**
 * Generated class for the TutorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

  user: any;
  userExist: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translate: TranslateService,
    public afAuth: AngularFireAuth,) {
      var user = firebase.auth().currentUser;

      if(user){
        this.userExist = true;
      } else {
        this.userExist = false;
      }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TutorialPage');
  }

  //BTN Vert et blanc
  skipTutorial(){
    this.navCtrl.popToRoot();
    this.checkRootPage();
  }

  //BTN Jaune
  goSignup(){
    this.navCtrl.push('SignupPage');
  }

  checkRootPage(){
    if(this.navCtrl.getPrevious() == null){
      this.navCtrl.setRoot(MapPage);
    }
  }



  /*loadData(){
    this.afAuth.authState.subscribe(async userf => {
      this.user = userf;
      if(this.user)  {

        var docRef = await firebase.firestore().collection('User').doc(this.user.uid);
        docRef.get().then(async doc => {
          if(doc.exists){
            this.userExist = true;
            console.log(this.userExist);
          }
        })
      }
      else{
        this.userExist = false;
        console.log(this.userExist);
        }
      });
    }*/
}
