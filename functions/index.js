// ====================================================================================================
//                             FIREBASE CREDENTIALS
// ====================================================================================================
const admin = require('firebase-admin');

const firebaseConfig = {
    apiKey: "AIzaSyD55-F9LSlUqqraQq4WxMxlSaJWHQSK2EY",
    authDomain: "fyzz-1536561400681.firebaseapp.com",
    databaseURL: "https://fyzz-1536561400681.firebaseio.com",
    projectId: "fyzz-1536561400681",
    storageBucket: "fyzz-1536561400681.appspot.com",
    messagingSenderId: "689686839605"
};
admin.initializeApp(firebaseConfig);

const functions = require('firebase-functions')
const stripe = require('stripe')(functions.config().stripe.token)


// ====================================================================================================
//                             CREATE STRIPE CUSTOMER
// ====================================================================================================
exports.createStripeCustomer = functions.region('europe-west1').auth.user().onCreate(function (user, context) {
    // register Stripe user
    return stripe.customers.create({
        email: user.email
    })
    .then(customer => {
      // update database with stripe customer id
      const data = { stripeId: customer.id }
      return admin.firestore().collection('User').doc(user.uid).update({stripeId: customer.id});
    })
});


// ====================================================================================================
//                             CREATE SOURCE
// ====================================================================================================
exports.createSource = functions.region('europe-west1').firestore.document("User/{userId}/Token/Token1").onCreate((data, context) => {
  const userId = context.params.userId;
  const newValue = data.data();
  customer = newValue.customerId;
  tokenId = newValue.tokenId;

  if (!tokenId) throw new Error('token missing');
  const response = stripe.customers.createSource(customer,{source: tokenId});
  if(response){
      return admin.firestore().collection('User').doc(userId).collection('Source').doc('Source1').set({status: 'source_add_successfully', customerId: customer});
  }else{
    throw new Error('source missing');
  }
});


// ====================================================================================================
//                             CREATE SOURCE NEW
// ====================================================================================================
exports.createSourceNew = functions.region('europe-west1').firestore.document("User/{userId}/Token/Token2").onCreate((data, context) => {
  const userId = context.params.userId;
  const newValue = data.data();
  customer = newValue.customerId;
  tokenId = newValue.tokenId;
  aboType = newValue.aboType;

  if (!tokenId) throw new Error('token missing');
  const response = stripe.customers.createSource(customer,{source: tokenId});
  if(response){
      return admin.firestore().collection('User').doc(userId).collection('Source').doc('Source2').set({status: 'source_add_successfully', customerId: customer, aboType: aboType});
  }else{
    throw new Error('source missing');
  }
});


// ====================================================================================================
//                             UPDATE SOURCE
// ====================================================================================================
exports.updateSource = functions.region('europe-west1').firestore.document("User/{userId}/Token/TokenUpdate").onCreate((data, context) => {
  const userId = context.params.userId;
  const newValue = data.data();
  customer = newValue.customerId;
  tokenId = newValue.tokenId;

  if (!tokenId) throw new Error('token missing');
  const response = stripe.customers.update(customer,{source: tokenId});
  if(response){
      return admin.firestore().collection('User').doc(userId).collection('Source').doc('SourceUpdate').set({status: 'source_update_successfully', customerId: customer});
  }else{
    throw new Error('source missing');
  }
});


// ====================================================================================================
//                             UPDATE SOURCE ON EXISTING TOKENUPDATE
// ====================================================================================================
exports.updateSourceExist = functions.region('europe-west1').firestore.document("User/{userId}/Token/TokenUpdate").onUpdate((change, context) => {
  const userId = context.params.userId;
  //const before = change.before.data()
  const after = change.after.data()

  // const newValue = data.data();
  customer = after.customerId;
  tokenId = after.tokenId;

  if (!tokenId) throw new Error('token missing');
  const response = stripe.customers.update(customer,{source: tokenId}); // ATTENTION la fonction createSource pas sûr. C'est pt customers.update
  if(response){
      return admin.firestore().collection('User').doc(userId).collection('Source').doc('SourceUpdate').set({status: 'source_update_successfully', customerId: customer});
  }else{
    throw new Error('source missing');
  }
});



// ====================================================================================================
//                             CREATE SUBSCRIPTION
// ====================================================================================================
exports.createSubscription = functions.region('europe-west1').firestore.document("User/{userId}/Source/Source1").onCreate((data, context) => {
const userId = context.params.userId;
const newValue = data.data();
customer = newValue.customerId;

return admin.firestore().collection(`/User`).get().then(c => {
  const count = c.size;

  // FYZZ 9.90/MOIS
    if(count <= 100 ){
      console.log("C'est le client " + count + ", c'est donc l'abo à 9.90/MOIS");
      const sub = stripe.subscriptions.create({
        customer: customer,
        //source: tokenId,
        items: [{ plan: 'plan_F902HtmU74FOTs'}]
      })

      if(sub){
        return admin.firestore().collection('User').doc(userId).collection('Stripe').doc('Abonnement').set({"fyzz_standard": "active"});
      }else{
        throw new Error('sub missing');
      }
    }else{

    // FYZZ 14.90/MOIS
      if(100 < count && count <= 500){
        console.log("C'est le client " + count + ", c'est donc l'abo à 14.90/MOIS");
        const sub = stripe.subscriptions.create({
          customer: customer,
          //source: tokenId,
          items: [{ plan: 'plan_F903eyh8ctpIJy'}]
        })

        if(sub){
          return admin.firestore().collection('User').doc(userId).collection('Stripe').doc('Abonnement').set({"fyzz_standard": "active"});
        }else{
          throw new Error('sub missing');
        }
      }else{

    // FYZZ 19.90/MOIS
        console.log("C'est le client " + count + ", c'est donc l'abo à 19.90/MOIS");
        const sub = stripe.subscriptions.create({
          customer: customer,
          //source: tokenId,
          items: [{ plan: 'plan_FAAzILTm4VByDm'}]
        })

        if(sub){
          return admin.firestore().collection('User').doc(userId).collection('Stripe').doc('Abonnement').set({"fyzz_standard": "active"});
        }else{
          throw new Error('sub missing');
        }
      }
    }
  })
//  ORIGINAL //
/*const sub = stripe.subscriptions.create({
    customer: customer,
    //source: tokenId,
    items: [{ plan: 'plan_Ey6Ps5L4qE4LwI'}]
    })

    if(sub){
      return admin.firestore().collection('User').doc(userId).collection('Stripe').doc('Abonnement').set({"fyzz_standard": "active"});
    }else{
      throw new Error('sub missing');
    }*/
});


// ====================================================================================================
//                             CREATE SUBSCRIPTION NEW
// ====================================================================================================
exports.createSubscriptionNew = functions.region('europe-west1').firestore.document("User/{userId}/Source/Source2").onCreate((data, context) => {
const userId = context.params.userId;
const newValue = data.data();
customer = newValue.customerId;
aboType = newValue.aboType;


// FYZZ 12.90/MOIS
  if(aboType == "12.90"){
    console.log("C'est un abo à 12.90/MOIS");
    const sub = stripe.subscriptions.create({
      customer: customer,
      //source: tokenId,
      items: [{ plan: 'plan_HEEE1bcwr6ZIcg'}]
    })

    if(sub){
      admin.firestore().collection('User').doc(userId).update({"fyzz_free": false});
      console.log("The user ID " + userId + " has set fyzz_free at false");
      return admin.firestore().collection('User').doc(userId).collection('Stripe').doc('Abonnement').set({"fyzz_standard": "active", "Abo_type": aboType});
    }else{
      throw new Error('sub missing');
    }
  }

// FYZZ 34.90/3 MOIS
  if(aboType == "34.90"){
    console.log("C'est un abo à 34.90/3 MOIS");
    const sub = stripe.subscriptions.create({
      customer: customer,
      //source: tokenId,
      items: [{ plan: 'plan_HEEF7IgRrd8y9P'}]
    })

    if(sub){
      admin.firestore().collection('User').doc(userId).update({"fyzz_free": false});
      console.log("The user ID " + userId + " has set fyzz_free at false");
      return admin.firestore().collection('User').doc(userId).collection('Stripe').doc('Abonnement').set({"fyzz_standard": "active", "Abo_type": aboType});
    }else{
      throw new Error('sub missing');
    }
  }

// FYZZ 59.90/ 6 MOIS
  if(aboType == "59.90"){
    console.log("C'est un abo à 59.90/6 MOIS");
    const sub = stripe.subscriptions.create({
      customer: customer,
      //source: tokenId,
      items: [{ plan: 'plan_HEEGQKln3rg3ST'}]
    })

    if(sub){
      admin.firestore().collection('User').doc(userId).update({"fyzz_free": false});
      console.log("The user ID " + userId + " has set fyzz_free at false");
      return admin.firestore().collection('User').doc(userId).collection('Stripe').doc('Abonnement').set({"fyzz_standard": "active", "Abo_type": aboType});
    }else{
      throw new Error('sub missing');
    }
  }
});



// ====================================================================================================
//                             RECURRING PAYMENT
// ====================================================================================================
exports.recurringPayment = functions.region('europe-west1').https.onRequest((req, res) => {
  const hook = req.body.type
  const data = req.body.data.object

  if (!data) throw new Error('missing data');

  const customer = data.customer;

  admin.firestore().collection('User').where("stripeId", '==', customer)
  .get()
  .then(querySnapshot =>{
    querySnapshot.forEach(function(doc) {
      if (hook === 'invoice.payment_succeeded') {
        return admin.firestore().collection('User').doc(doc.id).collection('Stripe').doc('Abonnement').update({"fyzz_standard": "active"})
        .then(() => res.status(200).send(`successfully handled ${hook}`))
        .catch(err => res.status(400).send(`error handling ${hook}`))
      }

      // Handle failed payment webhook
      if (hook === 'invoice.payment_failed') {
        return admin.firestore().collection('User').doc(doc.id).collection('Stripe').doc('Abonnement').update({"fyzz_standard": "pastDue"})
        .then(() => res.status(200).send(`successfully handled ${hook}`))
        .catch(err => res.status(400).send(`error handling ${hook}`))
      }

      // Handle card error webhook
      if (hook === 'payment_intent.payment_failed') {
        return admin.firestore().collection('User').doc(doc.id).collection('Stripe').doc('Abonnement').update({"fyzz_standard": "pastDue"})
        .then(() => res.status(200).send(`successfully handled ${hook}`))
        .catch(err => res.status(402).send(`error handling ${hook}`))
      }
    })
  });
});


// ====================================================================================================
//                             SOURCE STATUS
// ====================================================================================================
exports.sourceStatus = functions.region('europe-west1').https.onRequest((req, res) => {
  const hook = req.body.type
  const data = req.body.data.object

  if (!data) throw new Error('missing data');

  const customer = data.customer;

  admin.firestore().collection('User').where("stripeId", '==', customer)
  .get()
  .then(querySnapshot =>{
    querySnapshot.forEach(function(doc) {
      if (hook === 'customer.source.created') {
        return admin.firestore().collection('User').doc(doc.id).collection('Source').doc('SourceStatus').set({"sourceStatus": "ok"})
        .then(() => res.status(200).send(`successfully handled ${hook}`))
        .catch(err => res.status(400).send(`error handling ${hook}`))
      }else {
        return admin.firestore().collection('User').doc(doc.id).collection('Source').doc('SourceStatus').set({"sourceStatus": "error"})
        .then(() => res.status(200).send(`successfully handled`))
        .catch(err => res.status(400).send(`error handling`))
      }
    })
  });
});


// ====================================================================================================
//                             DELETE SUBSCRIPTION
// ====================================================================================================
exports.deleteSubscription = functions.region('europe-west1').https.onRequest((req, res) => {
  const hook = req.body.type
  const data = req.body.data.object

  if (!data) throw new Error('missing data');

  const customer = data.customer;

  admin.firestore().collection('User').where("stripeId", '==', customer)
  .get()
  .then(querySnapshot =>{
    querySnapshot.forEach(function(doc) {
      // Subscription deleted
      if (hook === 'customer.subscription.deleted') {
        admin.firestore().collection('User').doc(doc.id).collection('Source').listDocuments().then(val => {
          val.map((val) => {
              val.delete()
          })
        })
        admin.firestore().collection('User').doc(doc.id).collection('Stripe').listDocuments().then(val => {
          val.map((val) => {
              val.delete()
          })
        })
        admin.firestore().collection('User').doc(doc.id).collection('Token').listDocuments().then(val => {
          val.map((val) => {
              val.delete()
          })
        })

        return admin.firestore().collection('User').doc(doc.id).update({"fyzz_free": true})
        .then(() => res.status(200).send(`successfully handled ${hook}`))
        .catch(err => res.status(402).send(`error handling ${hook}`))
      }
    })
  });
});

// ====================================================================================================
//                             SCAN BUTTON ENABLE
// ====================================================================================================
exports.scheduledFunctionCrontab =
functions.region('europe-west1').pubsub.schedule('01 00 * * *')
.timeZone('Europe/Zurich')
.onRun((context) => {
    console.log('This will be run every day at 00:01 AM UTC!');
    return admin.firestore().collection('User')
    .get()
    .then(function(querySnapshot){
      var results = [];
      querySnapshot.forEach(function(doc) {
        var docRef = admin.firestore().collection('User').doc(doc.id);
        results.push(docRef.update({
          scannerDrinkBool: true,
          scannerAdvBool: true
        }));
      });
      return Promise.all(results);
    })
    .catch((error) => {
      console.log("Error getting document:", error);
    });
});

// ====================================================================================================
//                             fyzz_free BUTTON ENABLE
// ====================================================================================================
exports.scheduledFyzzFreeCrontab =
functions.region('europe-west1').pubsub.schedule('0 0 1 * *')
.timeZone('Europe/Zurich')
.onRun((context) => {
    console.log('This will be run on day-of-month 1 at 00:00 AM UTC!');
    return admin.firestore().collection('User')
    .get()
    .then(function(querySnapshot){
      var results = [];
      querySnapshot.forEach(function(doc) {
        var docRef = admin.firestore().collection('User').doc(doc.id);
        results.push(docRef.update({
          scannerAdvBoolSignup: true
        }));
      });
      return Promise.all(results);
    })
    .catch((error) => {
      console.log("Error getting document:", error);
    });
});
